extends Timer

var action
var object
var arg


class_name SDTimer

func _init(o=null,a=null,ar=null):
	action = a
	object = o
	arg = ar

func _ready():
	connect("timeout",self,"timeout")
	
func timeout():
	queue_free()
	if action and object:
		if arg:
			object.call(action,arg)
		else:
			object.call(action)
