extends Spatial

var fallspeed = 2
var accel = 0.15
var y_limit = - 20.0
var sp = AudioStreamPlayer3D.new()

func _ready():
	$Area.connect("body_entered",self,"_on_body_entered")
	sp.unit_size = 0.1
	sp.stream = preload("res://message.ogg")
	add_child(sp)
	sp.connect("finished",self,"queue_free")

func _on_body_entered(body):
	
	print ("yum!")
	sp.play()
	visible = false
	fallspeed=-1
	accel=0

func _process(delta):
	
	translate(fallspeed*delta*Vector3(0,-1,0))
	
	fallspeed+=accel
	
	if translation.y<=y_limit:
		queue_free()
