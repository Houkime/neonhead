tool

extends Spatial
class_name JumpPad

var swarmnode
export var color = Color8(40,180,200)
export var number = 3
export var left = true
export var sc = 0.07
export var dist = 1

var will_do_it=false # conflict resolution when player touches 2 simultaneously.

var anitime = 1

var pl

onready var vec = Vector3(1,0,0).rotated(Vector3(0,1,0),get_parent().rotation.y)

func _ready():
	swarmnode = Spatial.new()
	swarmnode.rotation_degrees = Vector3(-90, -90, 0)
	if !left:
		swarmnode.rotate_y(PI)
	add_child(swarmnode)
	for i in range(3):
		var a = Arrow.new(color)
		a.freq = 1
		a.translation = Vector3(i*dist - float(number)/2.0, 0, 0)
		a.rotation_degrees = Vector3(0, 0, -90)
		a.scale = Vector3(sc,sc,sc)
		swarmnode.add_child(a)



func deccel():
	print("deccel")
	accelerate(pl, "nop", pl.flyspeed, Vector3(0,0,0))
	
func accelerate(body, nextfunc, startval, endval):
	print("acc")
	var tw1 = SDTween.new(self,nextfunc)
	tw1.interpolate_property(body,"flyspeed", startval,endval,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(tw1)
	tw1.start()

func nop():
	print("noping")
	pl.flycrazy=false
	pl.find_node("NagPlace").nag()
	find_parent("MAIN").tried_to_escape = true

func take_responsibility():
	
	will_do_it = true
	var t = SDTimer.new(self,"drop_responsibility")
	t.wait_time = 1
	add_child(t)
	t.start()
	
func drop_responsibility():
	
	will_do_it = false
