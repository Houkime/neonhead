extends Tween
# Self-destructing Tween which doesn't need a separate cleanup.
# for convenience

var action
var object
var arg


class_name SDTween

func _init(o=null,a=null,ar=null):
	action = a
	object = o
	arg = ar

func _ready():
	connect("tween_completed",self,"timeout")
	
func timeout(arg1, arg2):
	queue_free()
	if action and object:
		if arg:
			object.call(action,arg)
		else:
			object.call(action)
