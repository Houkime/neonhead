extends Interactable


class_name Listenable

var chat

func start_chat():
	var c = Chat.new()
	chat = c
	$ChatPlace.add_child(c)
	return c 