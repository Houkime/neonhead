extends JumpPad

onready var pair = $"../JumpPadOut"

func _init():
	anitime = 0.5

func _on_Area_body_entered(body):
	#print(body.name+" entered JUMPZONE")
	if body.name=="Player":
		if !pair.will_do_it: 
			take_responsibility()
			body.flycrazy = true
			pl = body
			body.flyspeed = -vec
			
			accelerate(pl,"restore_player", body.flyspeed, Vector3(0,0,0))

func restore_player():
	
	pl.flycrazy=false
