extends Spatial

# governs game progression

var donutmat = preload("res://Donut_basemat.tres")
var icemat = preload("res://Donut_pink_icemat.tres")

class_name MainNode

var tried_to_escape = false

var baker_feeds_girl = false
var baker_feeds_drunkards = false
var drunkards_fed = false


var girl_fed = false
var girl_promised_to_return = false
var girl_didnt_return = false

var parenting_explained = false

var baker_secret = false
var baker_confessed = false

var bg_feeds_haxxor = false
var haxxor_fed = false
var hax_baker_init = false

var hax_safety_tips = false
var hax_salaryman = false

var seen_security_with_donut = false

var teacher_needed = false

var knows_printed = false
var knows_stack = false
var knows_drunkards = false

var knows_node = false

var knows_basic_anonimity = false

var security_explained = false

var warp_enabled = false

var knows_basic_safety = false
var knows_arsenic = false
var knows_lasagna_fun = false
var distrowatch = false #^____________________________^

var dna_scanned = false
var knows_girl_galaxies = false
var knows_synths = false

var priest_childhood_hook = false

var knows_girl_science = false

#var vip_operator_uncovered = false
var security_secret = false
var agitator_side = false
var lasagna_side = false
var haxxor_found = false
var security_knows = false
var agitator_knows = false
var girl_knows = false


func _ready():
	loader()

func loader():
	
	var counter = find_node("LOADER") # so that even if it is moved it is ok
	# counter can be anything really.
	# will probably be 3D at some point
	
	var scene_list = [
	"Agitator",
	"Donut",
	"HalfDonut",
	"Girl",
	"Priest",
	"BoozeGirl",
	"Haxxor",
	"Security",
	"Security Booth",
	"Head",
	"Human",
	"Clock",
	"Pads",
	
	]
	
	
	
	counter.max_items = scene_list.size()
	
	for sc in scene_list:
		load("res://"+sc+".tscn")
		counter.current_items += 1
		counter.update_counter()
	
	counter.visible = false
