extends Human

var speed = 0.4
var normal = Vector3(0,1,0)

func init_():
	
	return [
	Saying.new(Saying.ITSELF,"do not bother me"),
	Saying.new(Saying.ITSELF,"please"),
	]

func _process(delta):
	
	translation+=Vector3(0,0,speed*delta).rotated(normal,rotation.y)
