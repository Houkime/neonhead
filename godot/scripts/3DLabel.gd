extends Spatial

export var text = 'hello world'
var textnode
var letters_drawn = 0
var mat
export var color = Color8(100,100,100)
var power = 6.0
var rotspeed = 2.0

class_name Label3D


func _ready():
	prepare_material()
	make_text()

func make_text():
	if textnode:
		textnode.queue_free()
		textnode = null
	letters_drawn = 0
	
	textnode = Spatial.new()
	add_child(textnode)
	textnode.translation=Vector3(-float(text.length())*6.0/2.0,0.0,0.0)
	for c in text:
		add_letter(c)
	

func prepare_material():
	mat = SpatialMaterial.new()
	mat.emission_enabled=true
	mat.emission_energy = power
	mat.emission = color
	mat.albedo_color = ColorN("black")
	mat.albedo_texture = load("res://neon_test_texture0.png")

func add_letter(ch):
	if ch == " ":
		letters_drawn+=1
		return
	if ch == "!": ch = "exclamation"
	if ch == ".": ch = "dot"
	if ch == ",": ch = "comma"
	if ch == ":": ch = "colon"
	if ch == "\"": ch = "quotes"
	if ch == "'": ch = "apo"
	if ch == "?": ch = "question"
	var m = MeshInstance.new()
	ch=ch.to_lower()
	m.mesh = load("res://AlphaVoxel/"+ch+".obj")
	textnode.add_child(m)
	m.set_surface_material(0,mat)	
	m.translation = Vector3(letters_drawn*6.0,-5.0,0)
	letters_drawn+=1

