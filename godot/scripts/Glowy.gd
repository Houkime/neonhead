extends Spatial
class_name Glowy

var mat
export var color = Color8(100,100,100)
export var power = 6.0



func _ready():
	prepare_material()
	#print ("Glowy ready")

func prepare_material():
	mat = SpatialMaterial.new()
	mat.emission_enabled=true
	mat.emission_energy = power
	mat.emission = color
	mat.albedo_color = ColorN("black")
	mat.albedo_texture = load("res://neon_test_texture0.png")
