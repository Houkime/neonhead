extends Spatial

var mat
var color = ColorN("brown")
var power = 3.0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func prepare_material():
	mat = SpatialMaterial.new()
	mat.emission_enabled=true
	mat.emission_energy = power
	mat.emission = color
	mat.albedo_color = ColorN("black")
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	prepare_material()
	var m = MeshInstance.new()
	m.mesh = CubeMesh.new()
	m.mesh.size = Vector3(1,1,1)
	m.scale = Vector3(2.0,4.0,1)
	m.translation = Vector3(-1.5,2.5,-2.5)
	m.set_surface_material(0,mat)
	add_child(m)
