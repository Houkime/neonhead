extends Spatial

export var ending_name = "free"

var texts = {
	"free": "[path 1.1.1: freeee!!!]",
	"free_dna": "[path 1.1.2: kinda freee... but she has your dna.]",
	"along": "[path 1.2: come along with me]",
	"family": "[path 2: warm family]",
	"agi": "[path 3.1: war of good]",
	"dna": "[path 3.2: war of good. but she has your dna]",
	"biodeath": "[path 0: Live free or die. The last part.]",
}

func _ready():
	var sub = find_node("Subtitle")
	
	sub.text = texts[ending_name]
	sub.make_text()
