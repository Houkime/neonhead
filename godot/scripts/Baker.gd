extends Human
class_name Baker

var freq = 2
var phase = 0
var amplitude = 0.2

var float_up = false
var float_accel = 0.02
var floatspeed_max =1
var floatspeed = 0

var eye_mat = load("res://human_eyemat.tres")
var red_mat = load("res://saucemat.tres")
onready var eyes = find_node("eyes") as MeshInstance
onready var def_human_scale = $Spatial.scale.x

var angry = false

func _process(delta):
	if angry:
		$Spatial.scale = Vector3(1,1,1+amplitude*abs(sin(phase)))*def_human_scale
		phase=wrapf(phase+delta*6.28*freq,0,6.28)
	if float_up:
		if $Camera:
			var vec = Vector3(0,floatspeed*delta,0)
			$CameraPlace.translation+=vec
			$Camera.translation+=vec
			if floatspeed < floatspeed_max:
				floatspeed+=float_accel*delta

func _ready():
	pass

func get_angry():
	angry = true
	eyes.set_surface_material(0, red_mat)

func calm_down():
	angry = false
	$Spatial.scale = Vector3(1,1,1)*def_human_scale
	eyes.set_surface_material(0, eye_mat)

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hello"),
		Saying.new(Saying.ITSELF, "hi to you too!"),
		Saying.new(Saying.ITSELF, "want a donut or a pie?") if not (MAIN.girl_fed or MAIN.baker_feeds_girl) else NULL_SAY, 
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("no money","donut") if not (MAIN.girl_fed or MAIN.baker_feeds_girl) else NULL_CHOICE,
			Choice.new("why angry?","booze") if angry and not (MAIN.drunkards_fed or MAIN.baker_feeds_drunkards) else NULL_CHOICE,
			Choice.new("arsenic?","arsenic") if (MAIN.girl_fed or MAIN.baker_feeds_girl) else NULL_CHOICE,
			Choice.new("distro?","distro") if MAIN.distrowatch else NULL_CHOICE,
			Choice.new("what was in donut?","booze_arsenic") if (MAIN.drunkards_fed and MAIN.knows_arsenic) else NULL_CHOICE,
			Choice.new("enslavement","slave_") if MAIN.baker_secret else NULL_CHOICE,
			Choice.new("be my mommy?[End]", "mommy") if MAIN.baker_confessed and MAIN.parenting_explained else NULL_CHOICE, #TODO: maybe also sth connected with parents
			Choice.new("bye...", "bye_"),
		],	
	]

func donut_choice_():
	
	return [
		[
			Choice.new("CHNOPS","biochem_death"),
			Choice.new("CHNOAsS","right_donut"),
			Choice.new("SiO[sth]","biochem_death"),
			Choice.new("i better go", "bye_"),
		],	
	]

func privacy_choice_():
	
	return [
		[
			Choice.new("ok","ouch"),
			Choice.new("i refuse!","ok_cautious"),
			Choice.new("why?","donut_explanation"),
			Choice.new("i better go", "bye_"),
		],	
	]

func ok_cautious():
	
	return [
		Saying.new(Saying.MYSELF, "i will rather not"),
		Saying.new(Saying.ITSELF, "ok mr or mrs careful"),
		Saying.new(Saying.ITSELF, "choose yourself a donut"),
		
		Unconditional.new("donut_choice_"),
	]

func endgame():
	get_tree().change_scene("res://END2.tscn")
	pass

func endgame_biochem():
	get_tree().change_scene("res://END6.tscn")
	pass

func start_float():
	float_up = true



func donut_explanation():
	
	return [
		Saying.new(Saying.MYSELF, "why is it needed?"),
		Saying.new(Saying.ITSELF, "I need to know"),
		Saying.new(Saying.ITSELF, "your biochemistry"),
		Saying.new(Saying.ITSELF, "to choose a donut"),
		Saying.new(Saying.ITSELF, "edible by you"),
		
		Unconditional.new("privacy_choice_"),
	]

func biochem_death():
	
	return [
		Saying.new(Saying.MYSELF, "ohhh..."),
		Saying.new(Saying.ITSELF, "omg"),
		Saying.new(Saying.ITSELF, "here it goes again"),
		Saying.new(Saying.ITSELF, "sec!!!"),
		Saying.new(Saying.DRUNK, "[End the game]","endgame_biochem"),
	]

func right_donut(): #TODO: remove duplicate somewhere below
	
	return [
		Saying.new(Saying.ITSELF, "Oh! Same as her?"),
		Saying.new(Saying.ITSELF, "need some arsenic huh?"),
		Saying.new(Saying.ITSELF, "here..."),
		Saying.new(Saying.ITSELF, "take two and eat one", "give_donut"),
		Saying.new(Saying.MYSELF, "wow thx"),
		Saying.new(Saying.MYSELF, "it is delicious"),
		Saying.new(Saying.ITSELF, "i know, right?"),
	]



func ouch():
	
	return [
		Saying.new(Saying.MYSELF, "ooook?"),
		Saying.new(Saying.MYSELF, "ouch!!!","scan_DNA"),
		Saying.new(Saying.ITSELF, "Oh! Same as her?"),
		Saying.new(Saying.ITSELF, "need some arsenic huh?"),
		Saying.new(Saying.ITSELF, "here..."),
		Saying.new(Saying.ITSELF, "take two and eat one", "give_donut"),
		Saying.new(Saying.MYSELF, "wow thx"),
		Saying.new(Saying.MYSELF, "it is delicious"),
		Saying.new(Saying.ITSELF, "i know, right?"),
	]

func scan_DNA():
	MAIN.dna_scanned = true

func distro():
	return[
		Saying.new(Saying.MYSELF, "what distro"),
		Saying.new(Saying.MYSELF, "do you use?"),
		Saying.new(Saying.ITSELF, "oh?"),
		Saying.new(Saying.ITSELF, "just plain Bobonto"),
		Saying.new(Saying.ITSELF, "works ok"),
		Saying.new(Saying.ITSELF, "for bakery"),
		Saying.new(Saying.ITSELF, "doesn't matter"),
		
		Unconditional.new("main_choice_"),
	]

func mommy():
	
	return [
		Saying.new(Saying.MYSELF, "Will you be my mommy?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "... what??? ..."),
		Saying.new(Saying.MYSELF, "I have a bit of trouble"),
		Saying.new(Saying.MYSELF, "being on my own"),
		Saying.new(Saying.ITSELF, "you don't have parents?"),
		Saying.new(Saying.ITSELF, "i see."),
		Saying.new(Saying.ITSELF, "ok..."),
		Saying.new(Saying.ITSELF, "well"),
		Saying.new(Saying.ITSELF, "if you're not scared"),
		Saying.new(Saying.ITSELF, "by that girl episode"),
		Saying.new(Saying.ITSELF, "are you into this stuff?","start_float"),
		Saying.new(Saying.ITSELF, "by any chance?"),
		Saying.new(Saying.MYSELF, "idk yet."),
		Saying.new(Saying.MYSELF, "Are you even... sexy?"),
		Saying.new(Saying.ITSELF, "i believe i am!"),
		Saying.new(Saying.ITSELF, "no ugly grannies here"),
		########## Ending animations and whatnot while they are talking ######
		Saying.new(Saying.MYSELF, "can you cook?"),
		Saying.new(Saying.ITSELF, "are you stupid or what?"),
		Saying.new(Saying.MYSELF, "oh, forgot"),
		Saying.new(Saying.ITSELF, "don't worry"),
		Saying.new(Saying.ITSELF, "everything will be fine"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.DRUNK, "[End the game]"),
		Saying.new(Saying.DRUNK, "[End the game]","endgame"),
	]

func slave_():
	return [
		Saying.new(Saying.MYSELF, "is it true"),
		Saying.new(Saying.MYSELF, "that you tried"),
		Saying.new(Saying.MYSELF, "to enslave girl you fed?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "Sadly  it is"),
		Saying.new(Saying.ITSELF, "i am sorry kid"),
		Saying.new(Saying.ITSELF, "it is very easy"),
		Saying.new(Saying.ITSELF, "to get driven away"),
		Saying.new(Saying.ITSELF, "and stop discerning"),
		Saying.new(Saying.ITSELF, "between love and lust"),
		Saying.new(Saying.ITSELF, "at the last moment"),
		Saying.new(Saying.ITSELF, "i did realise"),
		Saying.new(Saying.ITSELF, "that i love her"),
		Saying.new(Saying.ITSELF, "and cancelled the thing", "confess"),
		Saying.new(Saying.MYSELF, "so what now?"),
		Saying.new(Saying.ITSELF, "idk"),
		Saying.new(Saying.ITSELF, "she"),
		Saying.new(Saying.ITSELF, "doesn't seem to like me"),
		Saying.new(Saying.ITSELF, "that's why i was anxious"),
		Saying.new(Saying.ITSELF, "but she likes my donuts"),
		Saying.new(Saying.ITSELF, "munches through them"),
		Saying.new(Saying.ITSELF, "while listening to head"),
		Saying.new(Saying.ITSELF, "so i guess"),
		Saying.new(Saying.ITSELF, "will just continue"),
		Saying.new(Saying.ITSELF, "making bakery for her"),
		Saying.new(Saying.ITSELF, "the least i can do"),
		Saying.new(Saying.ITSELF, "maybe one day"),
		Saying.new(Saying.ITSELF, "her heart will change"),
		Saying.new(Saying.ITSELF, "unlikely though"),
		]



func bye_():
	return [
		Saying.new(Saying.MYSELF, "have to go"),
		Saying.new(Saying.ITSELF, "take care"),
		]


func booze_arsenic():
	return [
		Saying.new(Saying.MYSELF, "you said"),
		Saying.new(Saying.MYSELF, "you need tissue"),
		Saying.new(Saying.MYSELF, "to understand"),
		Saying.new(Saying.MYSELF, "what you can sell."),
		Saying.new(Saying.MYSELF, "when you gave donut"),
		Saying.new(Saying.MYSELF, "to drunkards"),
		Saying.new(Saying.MYSELF, "you didn't have tissue"),
		Saying.new(Saying.MYSELF, "right?"),
		Saying.new(Saying.ITSELF, "hehe"),
		Saying.new(Saying.ITSELF, "they needed vodka."),
		Saying.new(Saying.ITSELF, "only more or less"),
		Saying.new(Saying.ITSELF, "baseline humans"),
		Saying.new(Saying.ITSELF, "can drink it"),
		Saying.new(Saying.ITSELF, "and get drunk"),
	]



func arsenic():
	return [
		Saying.new(Saying.MYSELF, "why do you say"),
		Saying.new(Saying.MYSELF, "i need arsenic?"),
		Saying.new(Saying.ITSELF, "Tissue samples"),
		Saying.new(Saying.ITSELF, "i don't want"),
		Saying.new(Saying.ITSELF, "to poison someone"),
		Saying.new(Saying.ITSELF, "accidentially"),
		Saying.new(Saying.ITSELF, "part of the business"),
		Saying.new(Saying.ITSELF, "quick test shows"),
		Saying.new(Saying.ITSELF, "your \"DNA\""),
		Saying.new(Saying.ITSELF, "or rather"),
		Saying.new(Saying.ITSELF, "whatever it became"),
		Saying.new(Saying.ITSELF, "is arsenic-based"),
		Saying.new(Saying.ITSELF, "and not exploding"),
		Saying.new(Saying.ITSELF, "only by a miracle."),
		Saying.new(Saying.ITSELF, "or rather"),
		Saying.new(Saying.ITSELF, "a full rewrite"),
		Saying.new(Saying.ITSELF, "of biochemistry."),
		Saying.new(Saying.ITSELF, "so i gave you"),
		Saying.new(Saying.ITSELF, "an optimised donut","know_arsenic"),
		Saying.new(Saying.ITSELF, "i need to make"),
		Saying.new(Saying.ITSELF, "quite a few variants"),
		Saying.new(Saying.ITSELF, "as you may imagine"),
		Saying.new(Saying.ITSELF, "human these days"),
		Saying.new(Saying.ITSELF, "is a very broad term"),
	]

func know_arsenic():
	MAIN.knows_arsenic = true

func donut():
	return [
		Saying.new(Saying.MYSELF, "no crypto"),
		Saying.new(Saying.ITSELF, "no worries"),
		Saying.new(Saying.ITSELF, "do me a favor instead"),
		Saying.new(Saying.ITSELF, "see that girl near Head?"),
		Saying.new(Saying.ITSELF, "give her one donut"),
		Saying.new(Saying.ITSELF, "from me"),
		Saying.new(Saying.ITSELF, "your hand for a sec?"),
		
		Unconditional.new("privacy_choice_"),
		]

func booze():
	return [
		Saying.new(Saying.MYSELF, "why are you so angry?"),
		Saying.new(Saying.ITSELF, "drunkards!"),
		Saying.new(Saying.ITSELF, "again!"),
		Saying.new(Saying.ITSELF, "scaring away clients!"),
		Saying.new(Saying.ITSELF, "...just jerks..."),
		Saying.new(Saying.ITSELF, "thing is"),
		Saying.new(Saying.ITSELF, "there are food synths"),
		Saying.new(Saying.ITSELF, "they are opensource"),
		Saying.new(Saying.ITSELF, "and energy is plenty!"),
		Saying.new(Saying.ITSELF, "all universe for them!"),
		Saying.new(Saying.ITSELF, "if they are hungry"),
		Saying.new(Saying.ITSELF, "or want to get drunk"),
		Saying.new(Saying.ITSELF, "no need to startle people","know_synths"),
		Saying.new(Saying.ITSELF, "...but still..."),
		Saying.new(Saying.ITSELF, "...they do..."),
		Saying.new(Saying.ITSELF, "here, take a donut", "give_donut_drunk"),
		Saying.new(Saying.ITSELF, "and tell them to get out!"),
		]

func know_synths():
	MAIN.knows_synths = true

func confess():
	MAIN.baker_confessed = true

func give_donut():
	PLAYER.take("Donut")
	MAIN.baker_feeds_girl = true
	
func give_donut_drunk():
	PLAYER.take("Donut")
	MAIN.baker_feeds_drunkards = true

