extends MeshInstance

var on = true
export var period = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var timer = Timer.new()
	timer.wait_time = period
	add_child(timer)
	timer.start()
	timer.connect("timeout",self,"toggle")

func toggle():
	on = not on
	update()

func update():
	get_surface_material(0).emission_enabled = on
	$OmniLight.visible= on