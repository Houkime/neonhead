extends Human
class_name Agitator


var freq = 1
var phase = 0
var active = true

var player_piggyback = false

onready var sign_ = find_node("Sign")

var previous_rotation

func _init():
	workplace = "Agipoint"
	transit_point = "TRANSIT1"

func _process(delta):
	if active: 
		sign_.translation = Vector3(0,0,abs(sin(phase)))
		phase=wrapf(phase+delta*6.28*freq,0,6.28)



func turn_back():
	turn_around(true)

func step_forward(l = 1.0):
		
		var tw2 = SDTween.new()
		tw2.interpolate_property(self,"translation",translation,translation+transform.basis.z*l,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		add_child(tw2)
		tw2.start()
	

func turn_around(back = false):
	PLAYER.dont_end_chat = true
	var tw = SDTween.new()
	
	force_finish_tweens()	
	
	if !back:
		previous_rotation = $Spatial.rotation_degrees
		tw.interpolate_property($Spatial,"rotation_degrees", previous_rotation, previous_rotation + Vector3(0,180,0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		step_forward()
	else:
		tw.interpolate_property($Spatial,"rotation_degrees", $Spatial.rotation_degrees, previous_rotation,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	
	add_child(tw)
	tw.start()

func force_finish_tweens():
	
	for c in get_children(): # not sure about that? 
	#What about cases when you climb on back and this tween gets deleted?
		if c is Tween:
			c.seek(c.get_runtime()) #hopefully sets tween to its end

func climb_back():
	player_piggyback = true
	var camera = $Camera
	var tw = SDTween.new()
	force_finish_tweens()
	var t 
	
	var cp = $CameraPlace
	t = cp.global_transform
	cp.get_parent().remove_child(cp)
	camera.add_child(cp)
	cp.global_transform = t

	t = camera.global_transform
	camera.get_parent().remove_child(camera)
	$Spatial.add_child(camera)
	camera.global_transform = t

	tw.interpolate_property(camera,"transform", camera.transform,$Spatial/PassengerPlace.transform, anitime, Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)

	add_child(tw)
	tw.start()

func take_player():
	pass

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "he..."),
		Saying.new(Saying.ITSELF, "hi! Wanna know more?"),
		Saying.new(Saying.MYSELF, "...llo"),
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("what are you doing?","platform"),
			Choice.new("priest","priest") if MAIN.lasagna_side else NULL_CHOICE,
			Choice.new("but there is a guard!","sec_argument") if MAIN.agitator_side and MAIN.security_explained else NULL_CHOICE,
			Choice.new("about girl","girl") if MAIN.baker_secret else NULL_CHOICE,
			Choice.new("she didn't return [END]","left") if MAIN.girl_didnt_return else NULL_CHOICE,
			Choice.new("what about stack?","stack") if MAIN.knows_stack else NULL_CHOICE,
			Choice.new("who are you?","who_are_you"),
			Choice.new("bye...", "bye_"),
		],	
	]


func left():
	
	return [
		Saying.new(Saying.MYSELF, "she said she needs to think"),
		Saying.new(Saying.MYSELF, "said will be today"),
		Saying.new(Saying.MYSELF, "but didn't return"),
		Saying.new(Saying.ITSELF, "oh"),
		Saying.new(Saying.ITSELF, "i guess that's it"),
		Saying.new(Saying.ITSELF, "she left"),
		Saying.new(Saying.ITSELF, "i will still try"),
		Saying.new(Saying.ITSELF, "to bring baker to justice"),
		Saying.new(Saying.ITSELF, "but for now"),
		Saying.new(Saying.ITSELF, "sec doesn't look happy"),
		Saying.new(Saying.ITSELF, "i guess we both"),
		Saying.new(Saying.ITSELF, "need to leave"),
		Saying.new(Saying.MYSELF, "i can't"), # TODO: wait to try escaping if didn't?
		Saying.new(Saying.MYSELF, "prints error"),
		Saying.new(Saying.ITSELF, "sorry"),
		Saying.new(Saying.ITSELF, "idk how to fix it"),
		Saying.new(Saying.ITSELF, "but let's try oldschool","turn_around"),
		Saying.new(Saying.ITSELF, "just climb me like that","climb_back"),
		Saying.new(Saying.ITSELF, "good","turn_back"), #camera moves on top of agi
		Saying.new(Saying.ITSELF, "let's go","leave"), # agi goes and warps. End
	]

func sec_argument():
	return [
		Saying.new(Saying.MYSELF, "hmm..."), #separate and tie to sec?
		Saying.new(Saying.MYSELF, "but there are antlers"),
		Saying.new(Saying.MYSELF, "look, over there?"),
		Saying.new(Saying.MYSELF, "You will have trouble"),
		Saying.new(Saying.MYSELF, "if you just hit me?"),
		Saying.new(Saying.ITSELF, "Well, sort of."),
		Saying.new(Saying.ITSELF, "but i can run away."),
		Saying.new(Saying.ITSELF, "He'll never find me"),
		Saying.new(Saying.ITSELF, "once i drop this sign"),
		Saying.new(Saying.ITSELF, "that's called anonymity","basic_anonymity"),
		Saying.new(Saying.ITSELF, "i am agitator"),
		Saying.new(Saying.ITSELF, "only when i need it"),
		Saying.new(Saying.MYSELF, "hmm, ok"),
		Saying.new(Saying.ITSELF, "besides"),
		Saying.new(Saying.ITSELF, "you think you know Sec?"),
		
		Unconditional.new("main_choice_"),
		]

func basic_anonymity():
	MAIN.knows_basic_anonimity = true

func platform():
	return [
		Saying.new(Saying.MYSELF, "what are you doing?"),
		Saying.new(Saying.ITSELF, "trying to awake people"),
		Saying.new(Saying.ITSELF, "net is in disarray."),
		Saying.new(Saying.ITSELF, "people go missing."),
		Saying.new(Saying.ITSELF, "what i think we need"),
		Saying.new(Saying.ITSELF, "is laws and justice"),
		Saying.new(Saying.ITSELF, "i.e. a state"),
		Saying.new(Saying.MYSELF, "laws?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "ok, for you"),
		Saying.new(Saying.ITSELF, "should start from basics"),
		Saying.new(Saying.ITSELF, "Long ago"),
		Saying.new(Saying.ITSELF, "before i was born"),
		Saying.new(Saying.ITSELF, "there were \"laws\""),
		Saying.new(Saying.ITSELF, "rules equal for all."),
		Saying.new(Saying.ITSELF, "for example a law"),
		Saying.new(Saying.ITSELF, "to not hurt other people"),
		Saying.new(Saying.ITSELF, "So i can't hit you"),
		Saying.new(Saying.ITSELF, "and get away with it"),
		Saying.new(Saying.ITSELF, "in public spaces"),
		Saying.new(Saying.ITSELF, "like this"),
		Saying.new(Saying.ITSELF, "there are rules to follow"),
		Saying.new(Saying.ITSELF, "however"),
		Saying.new(Saying.ITSELF, "enacting them is hard."),
		Saying.new(Saying.ITSELF, "and in other places"),
		Saying.new(Saying.ITSELF, "esp. in private ones"),
		Saying.new(Saying.ITSELF, "there are no rules at all"),
		Saying.new(Saying.ITSELF, "or twisted."),
		Saying.new(Saying.ITSELF, "No justice"),
		Saying.new(Saying.ITSELF, "you can be enslaved"),
		Saying.new(Saying.ITSELF, "tricked"),		
		Saying.new(Saying.ITSELF, "sold for organs"),
		Saying.new(Saying.ITSELF, "or just killed"),
		Saying.new(Saying.ITSELF, "millions of people"),
		Saying.new(Saying.ITSELF, "go missing every year"),
		Saying.new(Saying.ITSELF, "in just this segment"),
		Saying.new(Saying.ITSELF, "of the net"),
		Saying.new(Saying.ITSELF, "I cannot tolerate this","agiside"),
		
		Unconditional.new("main_choice_"),
		]


func agiside():
	MAIN.agitator_side = true

func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "good luck"),
		]

func stack():
	return [
		Saying.new(Saying.MYSELF, "what about stack?"),
		Saying.new(Saying.MYSELF, "it makes you safer?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "You might not feel this"),
		Saying.new(Saying.ITSELF, "yet"),
		Saying.new(Saying.ITSELF, "but tech stack"),
		Saying.new(Saying.ITSELF, "being the only thing"),
		Saying.new(Saying.ITSELF, "between you and death"),
		Saying.new(Saying.ITSELF, "is very stressfull."),
		Saying.new(Saying.ITSELF, "One wrong move"),
		Saying.new(Saying.ITSELF, "or one little bug"),
		Saying.new(Saying.ITSELF, "is all it may take"),
		Saying.new(Saying.ITSELF, "for a person to perish"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "I don't even understand"),
		Saying.new(Saying.ITSELF, "how the hell thing works"),
		Saying.new(Saying.ITSELF, "even though it is open"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "At least i know the risks"),
		Saying.new(Saying.ITSELF, "others just careless"),
		Saying.new(Saying.ITSELF, "they think it is magic"),
		Saying.new(Saying.ITSELF, "until they die."),
		Saying.new(Saying.ITSELF, "... this should end."),
		]


func priest():
	return [
		Saying.new(Saying.MYSELF, "priest says you're wrong"),
		Saying.new(Saying.ITSELF, "ah, Richard?"),
		Saying.new(Saying.ITSELF, "understandable"),
		Saying.new(Saying.ITSELF, "almost all of his \"church\""),
		Saying.new(Saying.ITSELF, "are foss and oshw devs."),
		Saying.new(Saying.ITSELF, "guys who make free tools"),
		Saying.new(Saying.ITSELF, "and release them wild"),
		Saying.new(Saying.ITSELF, "for both good and bad."),
		Saying.new(Saying.ITSELF, "the current net is"),
		Saying.new(Saying.ITSELF, "in many ways,"),
		Saying.new(Saying.ITSELF, "result of such behavior."),
		Saying.new(Saying.ITSELF, "I guess for Richard and co"),
		Saying.new(Saying.ITSELF, "being beyond control"),
		Saying.new(Saying.ITSELF, "is more important"),
		Saying.new(Saying.ITSELF, "than random casualties"),
		Saying.new(Saying.ITSELF, "caused by anarchy"),
		]

func girl():

	return [
		Saying.new(Saying.MYSELF, "I want to tell you sth"), #one needs to know she doesn't have parents prob.
		Saying.new(Saying.MYSELF, "This girl is in danger"),
		Saying.new(Saying.MYSELF, "right now"),
		Saying.new(Saying.ITSELF, "what?"),
		Saying.new(Saying.MYSELF, "baker woman"),
		Saying.new(Saying.MYSELF, "wanted to enslave her"),
		Saying.new(Saying.ITSELF, "what???"),
		Saying.new(Saying.MYSELF, "and haven't lost interest"),
		Saying.new(Saying.ITSELF, "this is a dark story."),
		Saying.new(Saying.ITSELF, "you're telling"),
		Saying.new(Saying.MYSELF, "how to protect her?"),
		Saying.new(Saying.ITSELF, "well, in such cases"),
		Saying.new(Saying.ITSELF, "the best protection"),
		Saying.new(Saying.ITSELF, "is usually publicity"),
		Saying.new(Saying.ITSELF, "I will contact friends"),
		Saying.new(Saying.ITSELF, "in nearby Zstate"),
		Saying.new(Saying.ITSELF, "and we will see"),
		Saying.new(Saying.ITSELF, "if they can grant"),
		Saying.new(Saying.ITSELF, "her public protection"),
		Saying.new(Saying.ITSELF, "in their space"),
		Saying.new(Saying.ITSELF, "until she grows up"),
		Saying.new(Saying.ITSELF, "what the girl says?"),
		Saying.new(Saying.ITSELF, "could you talk to her?","agiknows"),
		Saying.new(Saying.MYSELF, "can try"),
		Unconditional.new("main_choice_"),
	]


func who_are_you():

	return [
		Saying.new(Saying.MYSELF, "Who are you?"),
		Saying.new(Saying.ITSELF, "I am an activist"),
		Saying.new(Saying.ITSELF, "trying to make a change"),
		Unconditional.new("main_choice_"),
	]

func agiknows():
	MAIN.agitator_knows = true
