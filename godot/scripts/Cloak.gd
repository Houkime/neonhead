extends MeshInstance


class_name Cloak

var mat
var warp_power = 6
var warp_color = Color8(50,100,200)
var default_albedo

var in_sound = preload("res://sounds/humming_in.ogg")
var out_sound = preload("res://sounds/hmmming_out.ogg")

func _ready():
	default_albedo = get_surface_material(0).albedo_color
	prepare_warp_material()

func prepare_warp_material():
	mat = SpatialMaterial.new()
	mat.emission_enabled=true
	mat.emission = warp_color
	mat.emission_energy = 0
	mat.albedo_color = default_albedo
	mat.albedo_texture = load("res://neon_test_texture0.png")
	

func warp(anitime, incoming = false):
	set_surface_material(0,mat)
	var tw1 = SDTween.new()
	if incoming:
		tw1.interpolate_property(mat,"emission_energy", warp_power, 0,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		make_warpsound(in_sound,3)
	else:
		tw1.interpolate_property(mat,"emission_energy", 0, warp_power,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		make_warpsound(out_sound,2)
	add_child(tw1)
	tw1.start()


func make_warpsound(sound, volume = 2 ):
	var sp = SDStreamPlayer3D.new()
	sp.stream = sound
	sp.attenuation_model=AudioStreamPlayer3D.ATTENUATION_INVERSE_DISTANCE
	sp.unit_size = 0.1
	sp.unit_db = volume
	sp.play()
	add_child(sp)
