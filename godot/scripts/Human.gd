extends Chattable
class_name Human

var extension_max = 30
var anitime = 0.5
var footprint_size = 0.6

var warphook_in
var warphook_out


var workplace = null #nodename
var transit_point = null #nodename
# vvvv needed Pads node goes here when guest is called 
var queued_start = null #workaround for MAIN being busy and forcing deferred calls


func transit(target_transform, transit_transform = null):
	if transit_transform:
		go_to_point(transit_transform,self,"go_to_point",target_transform)
	else:
		go_to_point(target_transform)

func go_to_point(transf, cb_object = null, cb_method = null, cb_arg = null):	
	var tw
	
	tw = SDTween.new(cb_object,cb_method,cb_arg) #SDtween knows what to do with null args, no need to dupe
	tw.interpolate_property(self,"global_transform",global_transform,transf,1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	
	add_child(tw)
	tw.start()

func _ready():
	if queued_start:
		visible = false
		global_transform=queued_start.gl_inpoint()
		warp_in()

		queued_start=null
		# preventing player from interacting with things while they are moving
		_set_noninteractable()
		var t =SDTimer.new(self,"_set_interactable")
		t.wait_time = 3
		add_child(t)
		t.start()
	pass


func leave(exit = "Pads2"):
	_set_noninteractable()
	transit(MAIN.find_node(exit).gl_outpoint(), MAIN.find_node(transit_point).global_transform)

func go_to_workplace():
	transit(MAIN.find_node(workplace).global_transform, MAIN.find_node(transit_point).global_transform )


func _set_noninteractable():
	var colshape = $"Spatial/human"
	colshape.set_collision_layer_bit(0,false)
	colshape.set_collision_layer_bit(2,false)
	

func _set_interactable():
	var colshape = $"Spatial/human"
	colshape.set_collision_layer_bit(0,true)
	colshape.set_collision_layer_bit(2,true)
	

func warp(direction):
	var tw1 = SDTween.new(self,"warp_cleaning")
	var tw2 = SDTween.new()
	
	tw1.interpolate_property(self,"scale",Vector3(1,1,1), Vector3(1,1,extension_max), anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw2.interpolate_property(self,"translation",translation, translation+direction * extension_max*footprint_size, anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	
	for t in [tw1,tw2]:
		add_child(t)
		t.start()
	
	var cloak = find_node("human_robes")
	if cloak is Cloak:
		cloak.warp(anitime)
	if warphook_out:
		call(warphook_out)

func warp_cleaning():
	#print("cleaning from warp")
	
	if self.get("player_piggyback"): # when player is piggybacking
		print("property was found on an npc and active")
		
		if MAIN.dna_scanned:
			get_tree().change_scene("res://END5.tscn")
		else:
			get_tree().change_scene("res://END3.tscn")
		
	else:
		#print("player camera was not found")
		queue_free()

func warp_in():
	var direction
	var tw1 = SDTween.new(self,"go_to_workplace")
	var tw2 = SDTween.new()
	
	var initial_trans = translation
	
	translation = translation-transform.basis.z*extension_max*footprint_size
	
	tw1.interpolate_property(self,"scale",Vector3(1,1,extension_max), Vector3(1,1,1), anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw2.interpolate_property(self,"translation",translation, initial_trans, anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	
	for t in [tw1,tw2]:
		add_child(t)
		t.start()
	
	var cloak = find_node("human_robes")
	if cloak is Cloak:
		cloak.warp(anitime,true)
	if warphook_in:
		call(warphook_in)
	visible = true

