##This is just for showcasing stuff. No in-game purpose.

extends Camera

var point_at=Vector3()
var phi= 0.0
var siz = 10
const FREQ=0.3
var height = 0
var theta = 0

func _ready():
	point_at=Vector3(0,0.2,6)
	height = translation.y
	var dif = translation-point_at
	siz=Vector2(dif.x,dif.z).length()
	theta = -atan(height/siz)

func _process(delta):

	phi+=FREQ*2*PI*delta
	if (phi>=2*PI):
		phi=0
	translation=point_at+siz*Vector3(cos(phi),0,sin(phi))
	translation.y = height
	rotation=Vector3(theta,-phi+PI/2,0)
