extends Glowy

class_name Swayer

export var sway_freq = 0.3
export var sway_amplitude = 0.2
var xx = Vector3(1,0,0)
var zz = Vector3(0,0,1)
var sway_phase = 0

func _process(delta):
	var sway = sin(sway_phase)*sway_amplitude
	sway_phase=wrapf(sway_phase+delta*6.28*sway_freq,0,6.28)
	transform.basis = Basis(xx,Vector3(sway,1,0),zz)
