extends Spatial

func gl_inpoint():
	
	return find_node("JumpPadIn").find_node("orient").global_transform
	
func gl_outpoint():
	
	return find_node("JumpPadOut").find_node("orient").global_transform	
