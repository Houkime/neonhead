extends Listenable

var first_mono_done = false

onready var stabody = $"Spatial/Head"
onready var clock = find_parent("MAIN").find_node("Clock")

var monologue1 = [
	"\"...oh, really?\" he said",
	"and it was that moment",
	"when Konk finally realised",
	"that for his dreams", 
	"to come true",
	"all of this bs",
	"was completely unnecessary",
	"...",
	"ladies n gentlemen",
	"my neuromorphic overheated",
	"give me a sec",
]

var piece1 = [
	"\"gorgeously flying nuke\"",
	"read the fuselage sign",
	"sound of engines",
	"ripped ears apart",
	"\"are we still fertile?\"",
	"asked the boy",
	"\"idk\"",
	"\"more importantly\"",
	"\"build quality is crap\"",
	"\"can crash anytime\"",
	"\"right on us\"",
	"\"better go in subway\"",
]

var piece2 = [ #TODO: now that i think of it. Guests can provide interpretations and comments"
	"\"John, please\"",
	"\"stop poking people\"",
	"\"with your magic wand\"",
	"\"better go to elevator\"",
	"\"with me\"",
	"she said",
	"\"but...but...\"",
	"\"they are so pokey!\"",
	"\"can't stop!\"",
	"...",
	"\"that was president, John!\"",
	"\"don't leave him as a bat!\"",
	"accountant chorus:",
	"\"that's totally fine!\"",
	"\"leave him to us!\"",
]

var piece3 = [
	"her eyes were swampy green",
	"surrounded by lack of sleep",
	"and when she talked to me",
	"i knew",
	"she just wants",
	"the work done",
]

var piece4 = [
	"mirrored in the water",
	"there was a glowing spire",
	"distoring space and time",
	"\"idk what he is doing\"",
	"\"galaxy is big enough\"",
	"\"also\"",
	"\"why on a planet?\"",
	"but apparently",
	"HellSmith34",
	"thought otherwise",
]

var schedule = [
"piece1",
"piece2",
"piece3",
"piece4",
]


func _set_noninteractable():

	stabody.set_collision_layer_bit(2,false)
	

func _set_interactable():
	stabody.set_collision_layer_bit(2,true)

func root():
	if not first_mono_done:
		run_buffer("monologue1")
		chat.connect("buffer_ended",self,"mono1_end")
	else:
		run_buffer(schedule[clock.hour])
	

func run_buffer(buffer):
	chat.buffer = get(buffer)
	chat.read_buffer()

func mono1_end():
	print ("signal received")
	find_parent("MAIN").find_node("SECURITY",true).go_for_donut()
	first_mono_done = true
	_set_noninteractable() #Security.gd will switch it back when he is done
	


