extends Chattable

var ticktock = false

var hour = 0
var time = 0
var delay_timer
var spawn_delay = 1
var def_timespeed = 0.04
#var def_timespeed = 0.1
var timespeed = 0
var time_max = 1
var hour_max = 4
var times_used = 0
var ranted = false
var guests = Dictionary()
var startlock = true


var _friend = false

var light_swears = [ 
"ugh",
"umpf",
"grrrrr",]

var schedule = {
	"Agitator": [1],
	"Girl": [0,1],
	"Priest": [2],
	"BoozeGirl": [2],
	"Haxxor": [3],
	
}

func _set_noninteractable():
	$"Spatial/Clock/CollisionShape".disabled = true

func _set_interactable():
	$"Spatial/Clock/CollisionShape".disabled = false

func stop_time():
	timespeed = 0 
	#print("time stopped")
	
func start_time():
	_set_interactable()
	startlock = false
	timespeed = def_timespeed
	#print("time started")

func _ready():
	
	_on_hour_changed()
	_set_noninteractable()
	
	var timer = Timer.new()
	timer.wait_time = 1
	add_child(timer)
	timer.connect("timeout",self,"_tick")
	timer.start()

func _tick():
	
	if timespeed!=0:
		
		if ticktock:
			$Sound.pitch_scale = 0.8
		else:
			$Sound.pitch_scale = 1.5
		
		$Sound.play()
		ticktock = !ticktock


func _on_hour_changed(leap = false):
	
	if leap and delay_timer:
		delay_timer.queue_free()
	
	#make baker angry if BGs are around
	var baker = MAIN.find_node("BAKER")
	if hour in schedule["BoozeGirl"]:
		baker.get_angry()
	else:
		if baker.angry:
			baker.calm_down()
	
	#Remove late guests
	var kk = guests.keys().duplicate() #safety precaution. We're erasing stuff
	var i = 0
	for k in kk:
		if not (hour in schedule[k]) :
			if leap:
				guests[k].queue_free()
			else:
				guests[k].leave("Pads"+str(i+1))
			
			i = wrapi(i+1,0,2) # makes them use different exits if they are more than one
			guests.erase(k)
	
	
	#Add needed ones (delay prevents collisions)
	delay_timer = Timer.new()
	delay_timer.wait_time = spawn_delay
	add_child(delay_timer)
	delay_timer.connect("timeout",self,"populate_guests")
	delay_timer.start()


func populate_guests():
	
	if delay_timer:
		delay_timer.queue_free()
		delay_timer = null
		
	var i = 0
	
	for k in schedule.keys():
		if hour in schedule[k] and not k in guests.keys():
			
			call_guest(k,i+1)
			i = wrapi(i+1,0,2)
	


func call_guest(name_string, entrynumber = 1):
	
	var entrypoint = MAIN.find_node("Pads" + str(entrynumber))
	var guest
	
	if name_string == "Girl":
		if MAIN.girl_promised_to_return:
			MAIN.girl_didnt_return = true
			return
			
	guest = load("res://"+name_string+".tscn").instance()
	guest.queued_start = entrypoint
	guest.visible = false
	MAIN.call_deferred("add_child",guest) # guest will find his way from here
	#mainly because there is no way to do it otherwise with deferred calls
	# and deferred calls are forced by MAIN being busy
	guests[name_string] = guest


func _process(delta):
	time+=timespeed*delta
	if time > time_max :
		time = 0
		hour= wrapi(hour+1,0,hour_max)
		_on_hour_changed()
		
	$arrow_pivot.rotation_degrees.z = -((time/float(time_max))+hour)/(float(hour_max))*360

func init_():
	
	if ranted:
		return [
		Unconditional.new("main_choice_"),
		]
	if times_used>=light_swears.size():
		return [
		Unconditional.new("exploiter"),
		]
	else:
		return [
		Unconditional.new("manipulate_"),
		
		]

func exploiter():
	
	ranted = true
	
	return[
	
	Saying.new(Saying.ITSELF, "stop this already, man"),
	Saying.new(Saying.ITSELF, "you think"),
	Saying.new(Saying.ITSELF, "manipulating spacetime"),
	Saying.new(Saying.ITSELF, "is easy?"),
	Saying.new(Saying.ITSELF, "In this baroque"),
	Saying.new(Saying.ITSELF, "spatiotemporal mess"),
	Saying.new(Saying.ITSELF, "called net?"),
	Saying.new(Saying.ITSELF, "i am fed up."),
	Saying.new(Saying.ITSELF, "solve"),
	Saying.new(Saying.ITSELF, "your freaking Einsteins"),
	Saying.new(Saying.ITSELF, "and find needed tensors"),
	Saying.new(Saying.ITSELF, "yourself"),
	Saying.new(Saying.ITSELF, "little exploiter."),
	Saying.new(Saying.ITSELF, "destroy a bunch of nodes"),
	Saying.new(Saying.ITSELF, "while you do."),
	Saying.new(Saying.ITSELF, "will fix human demography"),
	Saying.new(Saying.ITSELF, "and make for less buggers"),
	Unconditional.new("main_choice_"),
	]


func main_choice_():
	return[
		[
		Choice.new("plz, need a warp!","manipulate_") if _friend else NULL_CHOICE,
		Choice.new("sec's friend?","friends") if _friend else NULL_CHOICE,
		Choice.new("wait, you talk?","intro"),
		Choice.new("why are you here?","reason"),
		Choice.new("bye","bye_"),
		],	
	]


func reason():
	return[
	
	Saying.new(Saying.MYSELF, "why are you here?"),
	Saying.new(Saying.MYSELF, "don't you have"),
	Saying.new(Saying.MYSELF, "more fun things to do"),
	Saying.new(Saying.MYSELF, "than to hang here?"),
	Saying.new(Saying.ITSELF, "I probably do"),
	Saying.new(Saying.ITSELF, "But for some reason"),
	Saying.new(Saying.ITSELF, "I just like to hang here"),
	Saying.new(Saying.ITSELF, "while doing other stuff"),
	Saying.new(Saying.ITSELF, "Sec recommended me place"),
	Saying.new(Saying.ITSELF, "to fight anxiety"),
	Saying.new(Saying.ITSELF, "was really peaceful here"),
	Saying.new(Saying.ITSELF, "until you showed up"),
	Saying.new(Saying.ITSELF, "little bugger"),
	Saying.new(Saying.ITSELF, "ordering me around"),
	Saying.new(Saying.ITSELF, "manipulate time!"),
	Saying.new(Saying.ITSELF, "1 hr forward!"),
	Saying.new(Saying.ITSELF, "2 hr forward!"),
	Saying.new(Saying.ITSELF, "what the hell?"),
	Saying.new(Saying.ITSELF, "you see a manual or sth?"),
	Saying.new(Saying.ITSELF, "do all clocks bend time?"),
	Saying.new(Saying.ITSELF, "on voice command?"),
	Saying.new(Saying.ITSELF, "i looked it up!"),
	Saying.new(Saying.ITSELF, "it was just you"),
	Saying.new(Saying.ITSELF, "making stuff up!"),
	Saying.new(Saying.ITSELF, "all the effing time!"),
	Saying.new(Saying.ITSELF, "jerk!", "unlock_sec"),
	Unconditional.new("main_choice_"),
	]

func unlock_sec():
	_friend = true
	
func sec_secret():
	MAIN.security_secret = true

func friends():
	return[
	
	Saying.new(Saying.MYSELF, "you're friends with Sec?"),
	Saying.new(Saying.ITSELF, "well i said it"),
	Saying.new(Saying.ITSELF, "we both just chillax"),
	Saying.new(Saying.ITSELF, "i am hanging as a clock"),
	Saying.new(Saying.ITSELF, "he is wordsmithing"),
	Saying.new(Saying.ITSELF, "and having an audience"),
	Saying.new(Saying.ITSELF, "as Head"),
	Saying.new(Saying.ITSELF, "both have fun."),
	Saying.new(Saying.MYSELF, "wait, i thought head"),
	Saying.new(Saying.MYSELF, "was a robot person"),
	Saying.new(Saying.MYSELF, "like you are"),
	Saying.new(Saying.MYSELF, "and it said sth"),
	Saying.new(Saying.MYSELF, "about neuromorphics"),
	Saying.new(Saying.MYSELF, "overheating"),
	Saying.new(Saying.MYSELF, "or sth"),
	Saying.new(Saying.ITSELF, "nah, all act"),
	Saying.new(Saying.ITSELF, "besides,"),
	Saying.new(Saying.ITSELF, "modern neuroboards"),
	Saying.new(Saying.ITSELF, "are very efficient"),
	Saying.new(Saying.ITSELF, "very little waste heat"),
	Saying.new(Saying.ITSELF, "passive cooling is enough"),
	Saying.new(Saying.ITSELF, "he just makes breaks"),
	Saying.new(Saying.ITSELF, "to eat human food"),
	Saying.new(Saying.ITSELF, "and relax a bit","sec_secret"),
	Unconditional.new("main_choice_"),
	]

func intro():
	return[
	
	Saying.new(Saying.MYSELF, "wait, you talk?"),
	Saying.new(Saying.ITSELF, "yea and what?"),
	Saying.new(Saying.MYSELF, "you can think?"),
	Saying.new(Saying.MYSELF, "you are so small!"),
	Saying.new(Saying.ITSELF, "Actually"),
	Saying.new(Saying.ITSELF, "your head is smaller"),
	Saying.new(Saying.ITSELF, "leave me alone"),
	Unconditional.new("main_choice_"),
	]



func manipulate_():
	return[
	
	Saying.new(Saying.MYSELF, "manipulate time!"),
	Saying.new(Saying.ITSELF, "what???") if ranted else NULL_SAY,
	Saying.new(Saying.ITSELF, "again???") if ranted else NULL_SAY,
	Saying.new(Saying.MYSELF, "pleeeease") if ranted else NULL_SAY,
	Saying.new(Saying.ITSELF, "jerk") if ranted else NULL_SAY,
	[
		Choice.new("1 hr forward","hr_1"),
		Choice.new("2 hr forward","hr_2"),
		Choice.new("quit","bye_"),
	],
	
	]


func bye_():
	return []

func hr_1():
	hour = wrapi(hour+1,0,hour_max)
	time = 0
	_on_hour_changed(true)
	return swear()

func hr_2():
	hour = wrapi(hour+2,0,hour_max)
	time = 0
	_on_hour_changed(true)
	return swear()

func swear():
	if ranted: return []
	times_used+=1
	return [
	Saying.new(Saying.ITSELF,light_swears[min(light_swears.size(),times_used-1)])
	]
