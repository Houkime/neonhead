extends Spatial

class_name Interactable

var epl = false
onready var PLAYER = find_parent("MAIN").find_node("Player")
var MAIN : MainNode 

func _ready():
	epl = find_node("EPlace")
	MAIN = find_parent("MAIN")

func bump_E():
	if epl:
		epl.visible = true
		epl.stay += 2
	
