extends KinematicBody

var speed = 0.4
var normal = Vector3(0.0,1.0,0.0)
var rotspeed = 0.01*1024.0
var theta_limit = PI/2.0*0.9
var camtheta = 0
var body_phi = 0
var cam_speed = Vector2(0,0)
var camera
var camera_menu_mode = false
var current_hand = ""
var flycrazy = false
var flyspeed = Vector3(1,0,0)
var maxflyspeed = 5.0
onready var camera_default_transform = $Camera.transform
onready var vp_size = get_viewport().size
var chat

var dont_end_chat = false

class_name Player

# Called when the node enters the scene tree for the first time.

func _input(event):
	if not camera_menu_mode:
		if event is InputEventMouseMotion:
			body_phi= wrapf(body_phi-event.relative.x * rotspeed/vp_size.x,-PI,PI)
			camtheta = clamp(camtheta - event.relative.y*rotspeed/vp_size.x,-theta_limit,theta_limit)
#		else:
#			print ("nonmouse event captured")
	if event.is_action_pressed("ui_cancel"):
		if not camera_menu_mode:
			#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			$NagPlace.ask_exit()
#		elif not dont_end_chat:
#			end_chat()

func end_chat():
	#print("esc pressed in menu mode")
	if !dont_end_chat:	
		var t = camera.global_transform
		camera.get_parent().remove_child(camera)
		add_child(camera)
		camera.global_transform = t
		camera_menu_mode = false
		#camera.translation,camera.rotation)
		var tw1 = SDTween.new()
		var camplace = camera_default_transform
		tw1.interpolate_property(camera,"transform",camera.transform,camplace,1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		add_child(tw1)
		tw1.start()
	
	chat.close()
	chat=null



func _process(delta):
	if !camera_menu_mode and !flycrazy:
		if Input.is_action_pressed("ui_down"):
			move_and_slide(Vector3(0,0,speed).rotated(normal,rotation.y),normal)
		if Input.is_action_pressed("ui_up"):
			move_and_slide(Vector3(0,0,-speed).rotated(normal,rotation.y),normal)
		if Input.is_action_pressed("ui_right"):
			move_and_slide(Vector3(speed,0,0).rotated(normal,rotation.y),normal)
		if Input.is_action_pressed("ui_left"):
			move_and_slide(Vector3(-speed,0,0).rotated(normal,rotation.y),normal)
	elif flycrazy:
		move_and_slide(flyspeed)
	
	var target = camera.get_node("RayCast").get_collider()
	
	if target!=null:
		var act_target = target.get_parent().get_parent()
		
		if !camera_menu_mode:
			if act_target is Interactable:
				act_target.bump_E()
				if Input.is_action_just_pressed("interact"):
					print(target.name if target!=null else "null")
					var c = camera
					var t = c.global_transform
					
					remove_child(c)
					act_target.add_child(c)
					c.global_transform = t
					camera_menu_mode = true
					var tw1 = SDTween.new()
					var camplace = act_target.get_node("CameraPlace")
					chat = act_target.start_chat()
					tw1.interpolate_property(c,"transform",
											c.transform,
											camplace.transform,
											1,
											Tween.TRANS_CUBIC,
											Tween.EASE_IN_OUT)
					add_child(tw1)
					tw1.start()
		
	
	if not camera_menu_mode:
		camera.rotation.x +=  (camtheta-camera.rotation.x)*0.3
		var dphi = body_phi - rotation.y
		if dphi>PI:
			dphi = dphi - 2*PI
		elif dphi<-PI:
			dphi = dphi + 2*PI
		dphi*=0.3
		rotation.y=wrapf(rotation.y+dphi,-PI,PI)
	
	

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	camera = $Camera
	pass # Replace with function body.

func take(itemname):
	if current_hand == "":
		current_hand = itemname
		var item = load("res://"+itemname+".tscn").instance()
		$Spatial.add_child(item)
		show_hand()

func give_hand():
	for c in $Spatial.get_children():
		c.queue_free()
	current_hand = ""

func show_hand():
	$Spatial.visible=true
