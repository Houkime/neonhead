extends Chattable

var freq = 0.5
var phase = 0
var active = true
var amplitude = 0.1

func _process(delta):
	if active: 
		$Spatial.translation = Vector3(0,amplitude*sin(phase),0)
		phase=wrapf(phase+delta*6.28*freq,0,6.28)


func init_():
	
	return [
	Saying.new(Saying.ITSELF, "ok","reset_"),
	]


	
func reset_():
	get_tree().change_scene("res://Main.tscn")

