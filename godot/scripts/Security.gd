extends Human
class_name Security

var fp

var idle_response_drunk = false
var idle_response_baker = false

onready var NOTEBOOK = find_parent("MAIN").find_node("Notebook")

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hi"),
		Saying.new(Saying.ITSELF, "hi kid"),
		Saying.new(Saying.ITSELF, "whazzup?"),
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("who are you?","who_are_you"),
			Choice.new("why antlers?", "antlers"),
			Choice.new("thing in front of you?", "notebook"),
			Choice.new("distro?","distro") if MAIN.distrowatch else NULL_CHOICE,
			Choice.new("rules?","rules") if MAIN.security_explained and MAIN.agitator_side else NULL_CHOICE,
			Choice.new("drunkards", "drunkards") if MAIN.knows_drunkards else NULL_CHOICE,
			Choice.new("about baker", "baker") if MAIN.baker_secret else NULL_CHOICE,
			Choice.new("why idle?", "idle") if idle_response_baker and idle_response_drunk else NULL_CHOICE,
			Choice.new("donut?", "donut") if MAIN.seen_security_with_donut else NULL_CHOICE,
			Choice.new("head is you?", "head_secret") if MAIN.security_secret else NULL_CHOICE,
			Choice.new("how to robofriend?", "robofriend") if MAIN.knows_node else NULL_CHOICE,
			Choice.new("cannot jump out", "jump_") if MAIN.tried_to_escape else NULL_CHOICE,
			
			Choice.new("bye...", "bye_"),
		],	
	]


func notebook():
	return[
		Saying.new(Saying.MYSELF, "what is this thing"),
		Saying.new(Saying.MYSELF, "on your table?"),
		Saying.new(Saying.ITSELF, "ah, this?"),
		Saying.new(Saying.ITSELF, "called a notebook"),
		Saying.new(Saying.ITSELF, "vintage machine"),
		Saying.new(Saying.ITSELF, "from linux era"),
		Saying.new(Saying.ITSELF, "but software is modern"),
		Saying.new(Saying.ITSELF, "all runs"),
		Saying.new(Saying.ITSELF, "just on 8 gigs"),
		Saying.new(Saying.ITSELF, "of ram!"),
		
		Unconditional.new("main_choice_"),
	]


func rules():
	return[
		Saying.new(Saying.MYSELF, "are there rules?"),
		Saying.new(Saying.MYSELF, "not to be poked?"),
		Saying.new(Saying.ITSELF, "not really rules"),
		Saying.new(Saying.ITSELF, "just"),
		Saying.new(Saying.ITSELF, "be excellent to others"),
		Saying.new(Saying.ITSELF, "there are not"),
		Saying.new(Saying.ITSELF, "so many people"),
		
		Unconditional.new("main_choice_"),
	]


func distro():
	return[
		Saying.new(Saying.MYSELF, "what distro"),
		Saying.new(Saying.MYSELF, "do you use?"),
		Saying.new(Saying.ITSELF, "i use nullportal"),
		Saying.new(Saying.ITSELF, "very minimal"),
		Saying.new(Saying.ITSELF, "it is rolling release"),
		Saying.new(Saying.ITSELF, "but bugs"),
		Saying.new(Saying.ITSELF, "almost nonexistent"),
		Saying.new(Saying.ITSELF, "strange if i haven't"),
		Saying.new(Saying.ITSELF, "told you that before"), # ^____^
		
		Unconditional.new("main_choice_"),
	]

func jump_():
	
	return [
		Saying.new(Saying.MYSELF, "can't jump out"),
		Saying.new(Saying.MYSELF, "error"),
		Saying.new(Saying.ITSELF, "really?"),
		Saying.new(Saying.ITSELF, "can be a malfunction"),
		Saying.new(Saying.ITSELF, "let me check logs"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "no, jumppad ok"),
		Saying.new(Saying.ITSELF, "it's on your side"),
		Saying.new(Saying.ITSELF, "check your setup"),
		Saying.new(Saying.MYSELF, "how?"),
		Saying.new(Saying.ITSELF, "idk, what distro?"),
		Saying.new(Saying.MYSELF, "don't know"),
		Saying.new(Saying.ITSELF, "uname r, dude"),
		Saying.new(Saying.ITSELF, "open your term"),
		Saying.new(Saying.MYSELF, "like... this?"),
		Saying.new(Saying.ITSELF, "what are you doing?"),
		Saying.new(Saying.ITSELF, "here, man! right here!"),
		Saying.new(Saying.MYSELF, "hmm... Bakano?"),
		Saying.new(Saying.ITSELF, "no idea how to fix"),
		Saying.new(Saying.ITSELF, "in Bakano"),
		Saying.new(Saying.ITSELF, "never used it"),
		Saying.new(Saying.ITSELF, "find smb else"), # very realistic situation, right? ^____^
		Saying.new(Saying.ITSELF, "btw"),
		Saying.new(Saying.ITSELF, "i use nullportal"),
		Saying.new(Saying.ITSELF, "very minimal"),
		Saying.new(Saying.ITSELF, "fewer errors"),
		
		Unconditional.new("main_choice_"),
	]


func head_secret():
	
	return [
		Saying.new(Saying.MYSELF, "clock says"),
		Saying.new(Saying.MYSELF, "head is your toy"),
		Saying.new(Saying.MYSELF, "why do you need it?"),
		Saying.new(Saying.ITSELF, "ah, you managed"),
		Saying.new(Saying.ITSELF, "to make hermit talk"),
		Saying.new(Saying.ITSELF, "cool"),
		Saying.new(Saying.ITSELF, "his name is Markus"),
		Saying.new(Saying.ITSELF, "has troubles socialising"),
		Saying.new(Saying.ITSELF, "well"),
		Saying.new(Saying.ITSELF, "this is an old node"),
		Saying.new(Saying.ITSELF, "hacked it a while back"),
		Saying.new(Saying.ITSELF, "was just lost in space"),
		Saying.new(Saying.ITSELF, "with defences up"),
		Saying.new(Saying.ITSELF, "like a wargame target"),
		Saying.new(Saying.ITSELF, "head's here from start"),
		Saying.new(Saying.ITSELF, "some socialist leader"),
		Saying.new(Saying.ITSELF, "just thought"),
		Saying.new(Saying.ITSELF, "would be cool."),
		Saying.new(Saying.ITSELF, "made it a makeover"),
		Saying.new(Saying.ITSELF, "etc"),
		Saying.new(Saying.ITSELF, "no particular reason", "knownode"),
		
		Unconditional.new("main_choice_"),
	]

func robofriend():

	return [
		Saying.new(Saying.MYSELF, "how can i get"),
		Saying.new(Saying.MYSELF, "a cool robot friend"),
		Saying.new(Saying.MYSELF, "like you have?"),
		Saying.new(Saying.MYSELF, "robot i met before"),
		Saying.new(Saying.MYSELF, "was strange"),
		
		Saying.new(Saying.ITSELF, "haha, man"),
		Saying.new(Saying.ITSELF, "they are all different"),
		Saying.new(Saying.ITSELF, "like humans"),
		Saying.new(Saying.ITSELF, "I met Markus"),
		Saying.new(Saying.ITSELF, "while travelling"),
		Saying.new(Saying.ITSELF, "in the machine net"),
		Saying.new(Saying.MYSELF, "machine net?"),
		Saying.new(Saying.ITSELF, "net of things, yep"),
		Saying.new(Saying.ITSELF, "space not optimised"),
		Saying.new(Saying.ITSELF, "for humans"),
		Saying.new(Saying.ITSELF, "though i wouldn't"),
		Saying.new(Saying.ITSELF, "call them things"),
		Saying.new(Saying.ITSELF, "there are persons also"),
		Saying.new(Saying.ITSELF, "like markus"),
		Saying.new(Saying.MYSELF, "how do you travel there?"),
		Saying.new(Saying.ITSELF, "you need some knowledge"),
		Saying.new(Saying.ITSELF, "and preferably"),
		Saying.new(Saying.ITSELF, "a neural interface"),
		Saying.new(Saying.MYSELF, "what?"),
		Saying.new(Saying.ITSELF, "brain connection to stack"),
		Saying.new(Saying.ITSELF, "this way"),
		Saying.new(Saying.ITSELF, "you're equal"),
		Saying.new(Saying.ITSELF, "2 robots with neuroboards"),
		Saying.new(Saying.ITSELF, "boardpeople"),
		Saying.new(Saying.ITSELF, "just a different network"),
		Saying.new(Saying.ITSELF, "and need oxygen"),
		Saying.new(Saying.MYSELF, "interface?"),
		Saying.new(Saying.MYSELF, "you mean... antlers?"),
		Saying.new(Saying.ITSELF, "haha, no"),
		Saying.new(Saying.ITSELF, "they are just for looks"),
		Saying.new(Saying.ITSELF, "sometimes to poke"),
		Saying.new(Saying.ITSELF, "interface is thin"),
		Saying.new(Saying.ITSELF, "you can't see it easily"),
		Saying.new(Saying.MYSELF, "can i have one?"),
		Saying.new(Saying.ITSELF, "of course you can"),
		Saying.new(Saying.ITSELF, "but probably better"),
		Saying.new(Saying.ITSELF, "when you really need it"),
		Saying.new(Saying.ITSELF, "at first"),
		Saying.new(Saying.ITSELF, "feels unnatural"),
		Saying.new(Saying.ITSELF, "causes depression"),
		Unconditional.new("main_choice_"),
	]


func knownode():
	MAIN.knows_node = true

func baker():
	
	idle_response_baker = true
	
	return [
		Saying.new(Saying.MYSELF, "did you know"),
		Saying.new(Saying.MYSELF, "that baker tried"),
		Saying.new(Saying.MYSELF, "to order capture"),
		Saying.new(Saying.MYSELF, "of little girl?"),
		Saying.new(Saying.ITSELF, "and then cancelled it?"),
		Saying.new(Saying.ITSELF, "m?"),
		Saying.new(Saying.ITSELF, "where it is coming from?"),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.ITSELF, "listen."),
		Saying.new(Saying.ITSELF, "don't be a dick"),
		Saying.new(Saying.ITSELF, "first"),
		Saying.new(Saying.ITSELF, "you need a proof"),
		Saying.new(Saying.ITSELF, "second"),
		Saying.new(Saying.ITSELF, "even if you've had it"),
		Saying.new(Saying.ITSELF, "still a dickery."),
		Saying.new(Saying.ITSELF, "if it was cancelled"),
		Saying.new(Saying.ITSELF, "what is it"),
		Saying.new(Saying.ITSELF, "you want to achieve?"),
		Saying.new(Saying.ITSELF, "if i ban baker"),
		Saying.new(Saying.ITSELF, "from this node"),
		Saying.new(Saying.ITSELF, "and the angry mob"),
		Saying.new(Saying.ITSELF, "of child protectors"),
		Saying.new(Saying.ITSELF, "will go everywhere"),
		Saying.new(Saying.ITSELF, "where bakey comes up"),
		Saying.new(Saying.ITSELF, "and this girl"),
		Saying.new(Saying.ITSELF, "won't listen to head"),
		Saying.new(Saying.ITSELF, "in peace"),
		Saying.new(Saying.ITSELF, "who will get happier?"),
		Saying.new(Saying.ITSELF, "if you are a dick"),
		Saying.new(Saying.ITSELF, "go speak to agitator"),
		Saying.new(Saying.ITSELF, "about this"),
		Saying.new(Saying.ITSELF, "but then"),
		Saying.new(Saying.ITSELF, "never go near me again"),
		Saying.new(Saying.ITSELF, "or this node"),
		Unconditional.new("main_choice_"),
	]



func antlers():
	
	return [
		Saying.new(Saying.MYSELF, "Why antlers?"),
		Saying.new(Saying.ITSELF, "they signify"),
		Saying.new(Saying.ITSELF, "my role as a mean guy"),
		Saying.new(Saying.ITSELF, "who keeps other meanies"),
		Saying.new(Saying.ITSELF, "out"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "with antlers","sec_explained"),
		Unconditional.new("main_choice_"),
	]

func sec_explained():
	MAIN.security_explained = true

func who_are_you():

	return [
		Saying.new(Saying.MYSELF, "Who are you?"),
		Saying.new(Saying.ITSELF, "name is Sec"),
		Saying.new(Saying.ITSELF, "I work here"),
		Saying.new(Saying.ITSELF, "call if in trouble","sec_explained"),
		Unconditional.new("main_choice_"),
	]

func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "see ya"),
		]

func donut():
	
	return [
		Saying.new(Saying.MYSELF, "I saw you with donut"),
		Saying.new(Saying.ITSELF, "emm. Yes?"),
		Saying.new(Saying.MYSELF, "when head took a break"),
		Saying.new(Saying.ITSELF, "he is a good storyteller"),
		Saying.new(Saying.ITSELF, "waited for him"),
		Saying.new(Saying.ITSELF, "to take a break"),
		Saying.new(Saying.ITSELF, "problem ?"),
		Unconditional.new("main_choice_"),
	]


func drunkards():
	
	idle_response_drunk = true
	
	return [
		Saying.new(Saying.MYSELF, "these drunk..."),
		Saying.new(Saying.ITSELF, "harmless"),
		Saying.new(Saying.MYSELF, "baker says"),
		Saying.new(Saying.MYSELF, "bad for clients"),
		Saying.new(Saying.ITSELF, "i am not her shop guard"),
		Saying.new(Saying.ITSELF, "besides"),
		Saying.new(Saying.ITSELF, "they are nice people"),
		Unconditional.new("main_choice_"),
	]


func idle():
	
	return [
		Saying.new(Saying.MYSELF, "you don't seem to do much"),
		Saying.new(Saying.ITSELF, "I don't?"),
		Saying.new(Saying.MYSELF, "well"),
		Saying.new(Saying.MYSELF, "it almost seems"),
		Saying.new(Saying.MYSELF, "like you are lazy"),
		Saying.new(Saying.MYSELF, "and invent reasons"),
		Saying.new(Saying.MYSELF, "not to do stuff"),
		Saying.new(Saying.MYSELF, "just admit it"),
		Saying.new(Saying.ITSELF, "weeeeell..."),
		Saying.new(Saying.ITSELF, "it is not really true"),
		Saying.new(Saying.ITSELF, "first"),
		Saying.new(Saying.ITSELF, "i prevent people hacking"),
		Saying.new(Saying.ITSELF, "you don't wanna"),
		Saying.new(Saying.ITSELF, "be on the node"),
		Saying.new(Saying.ITSELF, "when it gets hacked"),
		Saying.new(Saying.ITSELF, "trust me on this"),
		Saying.new(Saying.ITSELF, "second"),
		Saying.new(Saying.ITSELF, "if i rule here"),
		Saying.new(Saying.ITSELF, "with sec s iron hand"),
		Saying.new(Saying.ITSELF, "you won't like it"),
		Saying.new(Saying.ITSELF, "this is not a Sec land"),
		Saying.new(Saying.ITSELF, "this is a place for you all"),
		Saying.new(Saying.ITSELF, "to hang out and whatnot"),
		Saying.new(Saying.ITSELF, "just have fun"),
		Saying.new(Saying.MYSELF, "mmm. ok."),
		
		Unconditional.new("main_choice_"),
	]


func _ready():
	var sp = find_parent("MAIN").find_node("SecurityPath1",true)
	fp = sp.find_node("PathFollow")


##################### KINEMATIC FUNCTIONS ###############################

func receive_donut():
	var item = load("res://Donut.tscn").instance()
	$DonutPlace.add_child(item)

func delete_donut():
	for c in $DonutPlace.get_children():
		c.queue_free()

func go_for_donut():
	_set_noninteractable()
	NOTEBOOK.visible=false
	var tw1 = SDTween.new(self,"talk_with_baker")
	tw1.interpolate_property(fp,"unit_offset",0.01,.99,6,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(tw1)
	tw1.start()

func go_back():
	var tw1 = SDTween.new(self,"accomodate")
	tw1.interpolate_property(fp,"unit_offset",0.99,0.01,6,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(tw1)
	tw1.start()
	#self.rotation_degrees=Vector3(0,0,0)

func accomodate():
	_set_interactable()
	NOTEBOOK.visible=true
	MAIN.find_node("Head")._set_interactable()
	#print("getting comfy")
	delete_donut()
	var tw1 = SDTween.new()
	fp.rotation_degrees = Vector3(0,0,0)
	tw1.interpolate_property(self,"rotation_degrees",rotation_degrees,Vector3(0,0,0),1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(tw1)
	tw1.start()
	MAIN.seen_security_with_donut = true
	pass

func talk_with_baker():
	#print("talking with baker")
	receive_donut()
	var tw1 = SDTween.new(self,"go_back")
	tw1.interpolate_property(self,"rotation_degrees",rotation_degrees,Vector3(0,180,0),1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(tw1)
	tw1.start()
	
