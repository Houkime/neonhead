extends Glowy

class_name Arrow

var left = true
var meshnode
var mesh

var freq = 2
var phase = 0
var active = true

func _init(col = Color8(130,70,180)):
	self.color = col
	self.power=2.5

func _process(delta):
	if active: 
		meshnode.translation = Vector3(abs(sin(phase)),0,0)
		phase=wrapf(phase+delta*6.28*freq,0,6.28)
		
func _ready():
	mesh = MeshInstance.new()
	mesh.mesh = load("res://AlphaVoxel/more.obj")
	meshnode = Spatial.new()
	meshnode.add_child(mesh)
	mesh.set_surface_material(0,self.mat)
	mesh.translation = Vector3(-3.0,-5.0,0)
	if not left:
		self.rotation=Vector3(0,PI,0)
	add_child(meshnode)
	#print ("Arrow ready")
