extends Interactable

class_name Chattable
var chat
var NULL_CHOICE = Choice.new("","")
var NULL_SAY = Saying.new(Saying.MYSELF,"")
var rmargin = 0
var lmargin = 0
var vmargin = 0

func start_chat():
	var c = ChatPlane.new()
	chat = c
	$CameraPlace.add_child(c)
	
	return c 
