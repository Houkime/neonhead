extends Human
class_name Girl

var float_up = false
var float_accel = 0.02
var floatspeed_max =1
var floatspeed = 0


func _init():
	
	workplace = "Point3"
	transit_point = "TRANSIT2"

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hi"),
		Saying.new(Saying.ITSELF, "er... hi?"),
		Saying.new(Saying.ITSELF, "what is your deal?"),
		Unconditional.new("main_choice_"),
	]


func _process(delta):
	if float_up:
		if $Camera:
			var vec = Vector3(0,floatspeed*delta,0)
			$CameraPlace.translation+=vec
			$Camera.translation+=vec
			if floatspeed < floatspeed_max:
				floatspeed+=float_accel*delta

func main_choice_():
	
	return [
		[
			Choice.new("donut?","donut") if MAIN.baker_feeds_girl else NULL_CHOICE,
			Choice.new("distro?","distro") if MAIN.distrowatch and MAIN.girl_fed else NULL_CHOICE,
			Choice.new("science?","science") if MAIN.knows_girl_science and MAIN.girl_fed else NULL_CHOICE,
			Choice.new("show me a galaxy?[END]","come_along") if MAIN.knows_girl_galaxies and MAIN.girl_fed and MAIN.warp_enabled else NULL_CHOICE,
			Choice.new("parents are cool?","parents") if MAIN.parenting_explained and MAIN.girl_fed else NULL_CHOICE,
			Choice.new("state protection","protection") if MAIN.agitator_knows and MAIN.girl_knows else NULL_CHOICE,
			Choice.new("what to do?","doing") if MAIN.girl_knows else NULL_CHOICE,
			Choice.new("about baker","baker") if MAIN.baker_secret else NULL_CHOICE,
			Choice.new("who are you?","who_are_you"),
			Choice.new("bye...", "bye_"),
		],	
	]


func come_along():
	
	return[
		Saying.new(Saying.MYSELF, "i have just fixed"), # "I" ^___^
		Saying.new(Saying.MYSELF, "my stack"),
		Saying.new(Saying.MYSELF, "and can go anywhere"),
		Saying.new(Saying.MYSELF, "again"),
		Saying.new(Saying.MYSELF, "you said"),
		Saying.new(Saying.MYSELF, "i will freeze"),
		Saying.new(Saying.MYSELF, "if i see a galaxy?"),
		Saying.new(Saying.MYSELF, "from outside?"),
		Saying.new(Saying.ITSELF, "ahhh..."),
		Saying.new(Saying.ITSELF, "don't you wanna go home?"), #transparent hint to go away)))
		Saying.new(Saying.MYSELF, "well"),
		Saying.new(Saying.MYSELF, "i don't have one"),
		Saying.new(Saying.ITSELF, "why i even talked with you?"),
		Saying.new(Saying.ITSELF, "ok"),
		Saying.new(Saying.ITSELF, "we will go to my ship","start_float"),
		Saying.new(Saying.ITSELF, "and i'll give you a ride"),
		Saying.new(Saying.ITSELF, "you'll freeze"),
		Saying.new(Saying.ITSELF, "in a vacuum of space"),
		Saying.new(Saying.ITSELF, "and then"),
		Saying.new(Saying.ITSELF, "i can just listen"),
		Saying.new(Saying.ITSELF, "to head again"),
		Saying.new(Saying.ITSELF, "in wonderful solitude"),
		Saying.new(Saying.ITSELF, "ok?"),
		Saying.new(Saying.MYSELF, "suuuuuure"),
		Saying.new(Saying.ITSELF, "oh..."),
		Saying.new(Saying.DRUNK, "[End the game]"),
		Saying.new(Saying.DRUNK, "[End the game]","endgame"),
		
		Unconditional.new("main_choice_"),
	]

func start_float():
	float_up = true

func know_galaxies():
	MAIN.knows_girl_galaxies = true
	
func endgame():
	get_tree().change_scene("res://END4.tscn")
	pass


func science():
	
	return[
		Saying.new(Saying.MYSELF, "you said earlier"),
		Saying.new(Saying.MYSELF, "about science"),
		Saying.new(Saying.MYSELF, "what's fun exactly?"),
		Saying.new(Saying.ITSELF, "well"),
		Saying.new(Saying.ITSELF, "galaxies are just"),
		Saying.new(Saying.ITSELF, "very beautiful"),
		Saying.new(Saying.ITSELF, "if you see one live"),
		Saying.new(Saying.ITSELF, "from outside"),
		Saying.new(Saying.ITSELF, "you will just freeze","know_galaxies"),
		Saying.new(Saying.ITSELF, "literally"),
		Saying.new(Saying.ITSELF, "but sims do dynamics"),
		Saying.new(Saying.ITSELF, "that's an extra"),
		Saying.new(Saying.ITSELF, "and in math"),
		Saying.new(Saying.ITSELF, "puzzles unsolved"),
		Saying.new(Saying.ITSELF, "and stuff"),
		Saying.new(Saying.ITSELF, "that is hard"),
		Saying.new(Saying.ITSELF, "to imagine"),
		Saying.new(Saying.ITSELF, "waaaay deeper"),
		Saying.new(Saying.ITSELF, "then just physics"),
		Saying.new(Saying.ITSELF, "i like it"),
		Saying.new(Saying.ITSELF, "breath of mystery"),
		Saying.new(Saying.ITSELF, "almost religious"),
		Saying.new(Saying.ITSELF, "excites me"),
		
		Unconditional.new("main_choice_"),
	]

func distro():
	return[
		Saying.new(Saying.MYSELF, "what distro"),
		Saying.new(Saying.MYSELF, "do you use?"),
		Saying.new(Saying.ITSELF, "ScienceStone"),
		Saying.new(Saying.ITSELF, "cool to play with math"),
		Saying.new(Saying.ITSELF, "and physics"),
		Saying.new(Saying.ITSELF, "easy to make simulations"),
		Saying.new(Saying.ITSELF, "like simulating galaxies","know_science"), #TODO: maybe her scientific side needs more development
		Saying.new(Saying.ITSELF, "but all of these modules"),
		Saying.new(Saying.ITSELF, "for parallel calc"),
		Saying.new(Saying.ITSELF, "and zetabyte drives"),
		Saying.new(Saying.ITSELF, "make me look fat"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "handy though"),
		Saying.new(Saying.ITSELF, "can also mine crypto"),
		Saying.new(Saying.ITSELF, "and it's an unlimited"),
		Saying.new(Saying.ITSELF, "porn storage"),
		
		Unconditional.new("main_choice_"),
	]

func know_science():
	MAIN.knows_girl_science = true


func doing():
	return [
		
		Saying.new(Saying.MYSELF, "what"),
		Saying.new(Saying.MYSELF, "do you want me to do?"),
		Saying.new(Saying.ITSELF, "do whatever you want"),
		Saying.new(Saying.ITSELF, "by now"),
		Saying.new(Saying.ITSELF, "you probably want home"),
		Saying.new(Saying.ITSELF, "or sth"),
		]

func baker():
	return [
		Saying.new(Saying.MYSELF, "... excuse me"),
		Saying.new(Saying.MYSELF, "i want to tell you sth"),
		Saying.new(Saying.ITSELF, "then... talk?"),
		Saying.new(Saying.MYSELF, "well"),
		Saying.new(Saying.MYSELF, "recently i discovered"),
		Saying.new(Saying.MYSELF, "that baker"),
		Saying.new(Saying.MYSELF, "tried to hire people"),
		Saying.new(Saying.MYSELF, "to catch you"),
		Saying.new(Saying.MYSELF, "and make hers"),
		Saying.new(Saying.ITSELF, "ooof"),
		Saying.new(Saying.MYSELF, "but cancelled it"),
		Saying.new(Saying.ITSELF, "ok"),
		Saying.new(Saying.ITSELF, "kinda scary"),
		Saying.new(Saying.ITSELF, "but"),
		Saying.new(Saying.ITSELF, "she is not the first"),
		Saying.new(Saying.MYSELF, "what?"),
		Saying.new(Saying.ITSELF, "idk"),
		Saying.new(Saying.ITSELF, "it seems like i attract"),
		Saying.new(Saying.ITSELF, "some sort of people"),
		Saying.new(Saying.ITSELF, "and eventually"),
		Saying.new(Saying.ITSELF, "they go crazy"),
		Saying.new(Saying.ITSELF, "one after another"),
		Saying.new(Saying.MYSELF, "you are not scared?"),
		Saying.new(Saying.ITSELF, "i said it is scary"),
		Saying.new(Saying.ITSELF, "didn't you listen?"),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.ITSELF, "but only a bit"),
		Saying.new(Saying.ITSELF, "actually"),
		Saying.new(Saying.ITSELF, "it looks pathetic"),
		Saying.new(Saying.MYSELF, "but wait..."),
		Saying.new(Saying.MYSELF, "do you have any defence?"),
		Saying.new(Saying.MYSELF, "in case things get ugly?"),
		Saying.new(Saying.ITSELF, "well, not much"),
		Saying.new(Saying.ITSELF, "tech is complex for me"),
		Saying.new(Saying.ITSELF, "but what is there"),
		Saying.new(Saying.ITSELF, "to defend?"),
		Saying.new(Saying.ITSELF, "one way or another"),
		Saying.new(Saying.ITSELF, "wont feel anything","girlknow"),
		
		Unconditional.new("main_choice_"),
		]

func girlknow():
	MAIN.girl_knows = true

func parents():
	return [
		Saying.new(Saying.MYSELF, "do you have parents?"),
		Saying.new(Saying.MYSELF, "by any chance?"),
		Saying.new(Saying.MYSELF, "how is it like?"),
		Saying.new(Saying.MYSELF, "is it cool?"),
		Saying.new(Saying.ITSELF, "oh, you're printed too?"),
		Saying.new(Saying.ITSELF, "no, idk"),
		Saying.new(Saying.ITSELF, "never had them"),
		Saying.new(Saying.ITSELF, "sorry"),
		Unconditional.new("main_choice_"),
		]

func protection():
	return [
		Saying.new(Saying.MYSELF, "agitator says"),
		Saying.new(Saying.MYSELF, "Zstate can grant you"),
		Saying.new(Saying.MYSELF, "public protection"),
		Saying.new(Saying.MYSELF, "probably"),
		Saying.new(Saying.ITSELF, "you told him?"),
		Saying.new(Saying.ITSELF, "oh, well"),
		Saying.new(Saying.ITSELF, "i am not sure"),
		Saying.new(Saying.ITSELF, "it is a hard decision"),
		Saying.new(Saying.ITSELF, "need to think"),
		Saying.new(Saying.ITSELF, "lets talk tomorrow","promise"), #and she never returns
		Unconditional.new("main_choice_"),
		]

func promise():
	MAIN.girl_promised_to_return = true
	

func donut():
	return [
		Saying.new(Saying.MYSELF, "wanna donut?"),
		Saying.new(Saying.ITSELF, "this is new"),
		Saying.new(Saying.ITSELF, "a glowy donut!"),
		Saying.new(Saying.ITSELF, "why so generous?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "ah, i know"),
		Saying.new(Saying.ITSELF, "baker again"),
		Saying.new(Saying.MYSELF, "not first time?"),
		Saying.new(Saying.ITSELF, "kinda"),
		Saying.new(Saying.ITSELF, "but it is ok"),
		Saying.new(Saying.ITSELF, "will take it","take_donut"),
		Saying.new(Saying.ITSELF, "she is not too annoying"),
		Saying.new(Saying.MYSELF, "she?..."),
		Saying.new(Saying.ITSELF, "nvm"),
		Saying.new(Saying.ITSELF, "yummey!"),
		Unconditional.new("main_choice_"),
		]

func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "er... whatever"),
		]

func who_are_you():

	return [
		Saying.new(Saying.MYSELF, "Who are you?"),
		Saying.new(Saying.ITSELF, "leave me alone"),
		Saying.new(Saying.ITSELF, "I am here for art"),
		Saying.new(Saying.ITSELF, "not for adventures"),
		Unconditional.new("main_choice_"),
	]

func take_donut():
	PLAYER.give_hand()
	MAIN.girl_fed=true
	MAIN.find_node("Clock").startlock=false
	MAIN.baker_feeds_girl=false
