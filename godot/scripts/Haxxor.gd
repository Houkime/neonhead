extends Human
class_name Haxxor


func _init():
	
	workplace = "Point3"
	transit_point = "TRANSIT2"


func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hi"),
		Saying.new(Saying.ITSELF, "what is the matter?"),
		Saying.new(Saying.ITSELF, "i am busy"),
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("donut?","donut") if MAIN.bg_feeds_haxxor else NULL_CHOICE,
			Choice.new("computers?","comps") if MAIN.haxxor_fed and MAIN.hax_salaryman else NULL_CHOICE,
			Choice.new("baker?","baker") if MAIN.hax_baker_init else NULL_CHOICE,
			Choice.new("tissue tests?","tissue") if MAIN.knows_basic_safety else NULL_CHOICE,
			Choice.new("how to be safe?","safe") if MAIN.baker_secret else NULL_CHOICE,
			Choice.new("distro?","distro") if MAIN.distrowatch else NULL_CHOICE,
			Choice.new("government?","government") if MAIN.agitator_side and MAIN.hax_baker_init else NULL_CHOICE,
			Choice.new("safety possiblities?","safe_extended") if MAIN.knows_basic_safety else NULL_CHOICE,
			Choice.new("will you teach me?","teach") if MAIN.hax_safety_tips else NULL_CHOICE,
			Choice.new("why bright?","tie"),
			Choice.new("what will you buy?","money") if MAIN.hax_salaryman else NULL_CHOICE,
			Choice.new("bye...", "bye_"),
		],	
	]

func distro():
	return[
		Saying.new(Saying.MYSELF, "what distro"),
		Saying.new(Saying.MYSELF, "do you use?"),
		Saying.new(Saying.ITSELF, "PokePoke"),
		Saying.new(Saying.ITSELF, "but that's just me"),
		
		Unconditional.new("main_choice_"),
	]


func tissue(): 
	return [
		Saying.new(Saying.MYSELF, "baker takes tissue samples"),
		Saying.new(Saying.MYSELF, "from visitors"),
		Saying.new(Saying.ITSELF, "cool"),
		Saying.new(Saying.ITSELF, "privacy"),
		Saying.new(Saying.ITSELF, "right out of the window"),
		Saying.new(Saying.ITSELF, "she can track them"),
		Saying.new(Saying.ITSELF, "and sell info to others"),
		Saying.new(Saying.ITSELF, "very hard to change DNA"),
		Saying.new(Saying.ITSELF, "or whatever you have"),
		
		Unconditional.new("main_choice_"),
	]

func government(): 
	return [
		Saying.new(Saying.MYSELF, "what go you think"),
		Saying.new(Saying.MYSELF, "about government?"),
		Saying.new(Saying.ITSELF, "nah"),
		Saying.new(Saying.ITSELF, "it is for lame dudes"),
		Saying.new(Saying.ITSELF, "who can't"),
		Saying.new(Saying.ITSELF, "govern themselves"),
		Saying.new(Saying.ITSELF, "let alone machines"),
		Saying.new(Saying.ITSELF, "more of a placebo really"),
		Saying.new(Saying.ITSELF, "takes away freedom."),
		Saying.new(Saying.ITSELF, "grants false security."),
		Saying.new(Saying.ITSELF, "is constantly attacked."),
		Saying.new(Saying.ITSELF, "typical country"),
		Saying.new(Saying.ITSELF, "lives for a year or so"),
		Saying.new(Saying.ITSELF, "even if hidden"),
		Saying.new(Saying.ITSELF, "own citizens destroy it"),
		Saying.new(Saying.ITSELF, "try look for yourself"),
		Saying.new(Saying.ITSELF, "you can quit anytime"),
		Saying.new(Saying.ITSELF, "...usually..."),
		Saying.new(Saying.ITSELF, "also if really bored"),
		Saying.new(Saying.ITSELF, "destroy one from inside"),
		Saying.new(Saying.ITSELF, "nice hacking practice"),
		
		Unconditional.new("main_choice_"),
	]

func safe_extended(): #TODO: freaking long monologue! Do sth with it! Even making more chars will help with dullness and will help with the story.
	return [
		Saying.new(Saying.MYSELF, "how safe i can be?"),
		Saying.new(Saying.ITSELF, "completely"),
		Saying.new(Saying.ITSELF, "buuut price incurs"),
		Saying.new(Saying.ITSELF, "ok, most rad variant"),
		Saying.new(Saying.ITSELF, "one can separate"),
		Saying.new(Saying.ITSELF, "to its own universe"),
		Saying.new(Saying.ITSELF, "and be all alone"),
		Saying.new(Saying.ITSELF, "forever"),
		Saying.new(Saying.ITSELF, "but unable to come back"),
		Saying.new(Saying.ITSELF, "or, less rad, live"),
		Saying.new(Saying.ITSELF, "outside of net"),
		Saying.new(Saying.ITSELF, "among roids"),
		Saying.new(Saying.ITSELF, "near unknown star"),
		Saying.new(Saying.ITSELF, "and be several months"),
		Saying.new(Saying.ITSELF, "away from the rest"),
		Saying.new(Saying.ITSELF, "hidden"),
		Saying.new(Saying.ITSELF, "and growing children"),
		Saying.new(Saying.ITSELF, "I was grown like this!"),
		Saying.new(Saying.ITSELF, "might find aliens:]"),
		Saying.new(Saying.ITSELF, "if lucky."),
		Saying.new(Saying.ITSELF, "or one can make"),
		Saying.new(Saying.ITSELF, "an own node"),
		Saying.new(Saying.ITSELF, "independent and stealthy"),
		Saying.new(Saying.ITSELF, "connecting at will"),
		Saying.new(Saying.ITSELF, "for example to friends"),
		Saying.new(Saying.ITSELF, "and be on guard"),
		Saying.new(Saying.ITSELF, "and relocate often"),
		Saying.new(Saying.ITSELF, "avoiding detection"),
		Saying.new(Saying.ITSELF, "and hackers"),
		Saying.new(Saying.ITSELF, "and for all of this"),
		Saying.new(Saying.ITSELF, "you just need your stack!"),
		Saying.new(Saying.ITSELF, "for example"),
		Saying.new(Saying.ITSELF, "did you know"),
		Saying.new(Saying.ITSELF, "that you can create"),
		Saying.new(Saying.ITSELF, "millions of tech gadgets?"),
		Saying.new(Saying.ITSELF, "like"),
		Saying.new(Saying.ITSELF, "machines to make nodes"),
		Saying.new(Saying.ITSELF, "food and water?"),
		Saying.new(Saying.MYSELF, "whaaaat?"),
		Saying.new(Saying.ITSELF, "yup. also defence systems"),
		Saying.new(Saying.ITSELF, "printers"),
		Saying.new(Saying.ITSELF, "spaceships"),
		Saying.new(Saying.ITSELF, "stargates"),
		Saying.new(Saying.ITSELF, "other people"),
		Saying.new(Saying.ITSELF, "and not really people"),
		Saying.new(Saying.ITSELF, "and pretty much anything else"),
		Saying.new(Saying.ITSELF, "humanity has invented"),
		Saying.new(Saying.ITSELF, "over hundreds of years"),
		Saying.new(Saying.ITSELF, "plus your own stuff"),
		Saying.new(Saying.ITSELF, "that doesn't exist yet"),
		Saying.new(Saying.MYSELF, "how can i do that?"),
		Saying.new(Saying.ITSELF, "by reading documentation","hacktips"), # TODO: possibly flag change needs to be moved to basics
		Saying.new(Saying.ITSELF, "it is included."),
		Saying.new(Saying.ITSELF, "be a bit careful."),
		Saying.new(Saying.ITSELF, "it is not idiotproof"),
		Saying.new(Saying.ITSELF, "easy to kill yourself"),
		Saying.new(Saying.ITSELF, "that's it"),
		
		Unconditional.new("main_choice_"),
		]

func know_basics():
	MAIN.knows_basic_safety = true

func safe():
	return [
		Saying.new(Saying.MYSELF, "so how one can be safe?"),
		Saying.new(Saying.MYSELF, "baker ordered that girl"),
		Saying.new(Saying.MYSELF, "i am a kid too"),
		Saying.new(Saying.MYSELF, "what do i need to do"),
		Saying.new(Saying.MYSELF, "not to become a slave?"),
		Saying.new(Saying.ITSELF, "valid question"),
		Saying.new(Saying.ITSELF, "so serious mode now :]"),
		Saying.new(Saying.ITSELF, "...grhm..."),
		Saying.new(Saying.ITSELF, "in the order"),
		Saying.new(Saying.ITSELF, "she was described"),
		Saying.new(Saying.ITSELF, "as a kid visiting head"),
		Saying.new(Saying.ITSELF, "girl was doing it"),
		Saying.new(Saying.ITSELF, "very regularly."),
		Saying.new(Saying.ITSELF, "even if she were bigger"),
		Saying.new(Saying.ITSELF, "her behavior differs."),
		Saying.new(Saying.ITSELF, "so mixing with crowd"),
		Saying.new(Saying.ITSELF, "and anonymizing"),
		Saying.new(Saying.ITSELF, "can't really work"),
		Saying.new(Saying.ITSELF, "emotions"),
		Saying.new(Saying.ITSELF, "like love for art"),
		Saying.new(Saying.ITSELF, "lead to mistakes"),
		Saying.new(Saying.ITSELF, "so for start"),
		Saying.new(Saying.ITSELF, "control yourself"),
		Saying.new(Saying.ITSELF, "...I wish i could :]"),
		Saying.new(Saying.ITSELF, "...grhm..."),
		Saying.new(Saying.ITSELF, "second thing is tech"),
		Saying.new(Saying.ITSELF, "anyone is in trouble"),
		Saying.new(Saying.ITSELF, "if one does not know"),
		Saying.new(Saying.ITSELF, "how stuff really works"),
		Saying.new(Saying.ITSELF, "her defences are weak"),
		Saying.new(Saying.ITSELF, "poorly set"),
		Saying.new(Saying.ITSELF, "easy to bypass"),
		Saying.new(Saying.ITSELF, "unlike lasagna"),
		Saying.new(Saying.ITSELF, "filled with fun!"),
		Saying.new(Saying.ITSELF, "...also bloated as hell","know_lasagna"),
		Saying.new(Saying.ITSELF, "...grhm..."),
		Saying.new(Saying.ITSELF, "the rest depends on"),
		Saying.new(Saying.ITSELF, "effort and sacrifices"),
		Saying.new(Saying.ITSELF, "on how far you wanna go","know_basics"),
		
		Unconditional.new("main_choice_"),
		]

func know_lasagna():
	MAIN.knows_lasagna_fun = true

func hacktips():
	MAIN.hax_safety_tips = true
	MAIN.teacher_needed = true

func teach():
	return [
		Saying.new(Saying.MYSELF, "will you teach me?"),
		Saying.new(Saying.ITSELF, "nah"),
		Saying.new(Saying.ITSELF, "i don't have time"),
		Saying.new(Saying.ITSELF, "sorry kid"),
		Saying.new(Saying.ITSELF, "will take too long"),
		Saying.new(Saying.ITSELF, "lasagna priests"),
		Saying.new(Saying.ITSELF, "like to teach"),
		Saying.new(Saying.ITSELF, "but better"),
		Saying.new(Saying.ITSELF, "learn yourself"),
		Saying.new(Saying.ITSELF, "just find a place"),
		
		Unconditional.new("main_choice_"),
		]

func money():
	return [
		Saying.new(Saying.MYSELF, "What would you buy?"),
		Saying.new(Saying.MYSELF, "On your salary?"),
		Saying.new(Saying.ITSELF, "Oh, i will tell you!"),
		Saying.new(Saying.ITSELF, "I want a personal chef,"),
		Saying.new(Saying.ITSELF, "Antique art collection,"),
		Saying.new(Saying.ITSELF, "Awesome closed software"),
		Saying.new(Saying.ITSELF, "with sparky bloat,"),
		Saying.new(Saying.ITSELF, "And lots of girls!"),#interpret how you want. no reason for haxxor to strive for clarity here and dwell into details.
		Saying.new(Saying.ITSELF, "Will have cool photos"),
		Saying.new(Saying.ITSELF, "in my blog"),
		Saying.new(Saying.ITSELF, "And feel myself classy."),
		Saying.new(Saying.ITSELF, "for that to happen sooner"),
		Saying.new(Saying.ITSELF, "head to FurnitureCo node"),
		Saying.new(Saying.ITSELF, "And buy yourself"),
		Saying.new(Saying.ITSELF, "A super comfy chair!"),
		
		Unconditional.new("main_choice_"),
		]

func donut():
	return [
		Saying.new(Saying.MYSELF, "drunkard"),
		Saying.new(Saying.MYSELF, "wants you to eat this"),
		Saying.new(Saying.ITSELF, "mmmm..."),
		Saying.new(Saying.ITSELF, "luminescent pink donut"),
		Saying.new(Saying.ITSELF, "so cute of her"),
		Saying.new(Saying.ITSELF, "ok"),
		Saying.new(Saying.MYSELF, "...she?..."),
		Saying.new(Saying.MYSELF, "is she your gf or sth?"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "unfortunately not"),
		Saying.new(Saying.ITSELF, "hates code and hardware","take_donut"),
		Saying.new(Saying.ITSELF, "i am too boring for her"),
		Saying.new(Saying.MYSELF, "so why feeding you?"),
		Saying.new(Saying.ITSELF, "idk"),
		
		Unconditional.new("main_choice_"),
		]

func baker():
		return [
		Saying.new(Saying.MYSELF, "baker?"),
		Saying.new(Saying.ITSELF, "ah?"),
		Saying.new(Saying.ITSELF, "afterwards"),
		Saying.new(Saying.ITSELF, "it was cancelled","baker_secret_complete"),
		Saying.new(Saying.ITSELF, "i guess change of mind"),
		Saying.new(Saying.MYSELF, "so... baker is bad?"),
		Saying.new(Saying.ITSELF, "idk"),
		Saying.new(Saying.ITSELF, "humans are complex"),
		Saying.new(Saying.ITSELF, "\"bad\" is subjective"),
		
		Unconditional.new("main_choice_"),
		]

func comps():
	return [
		Saying.new(Saying.MYSELF, "wait a sec"),
		Saying.new(Saying.MYSELF, "you said"),
		Saying.new(Saying.MYSELF, "you sell furniture"),
		Saying.new(Saying.ITSELF, "yes?"),
		Saying.new(Saying.MYSELF, "but you also said"),
		Saying.new(Saying.MYSELF, "about code and hw?"),
		Saying.new(Saying.ITSELF, "we are modern company..."),
		Saying.new(Saying.MYSELF, "dots"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.MYSELF, "many dots!"),
		Saying.new(Saying.ITSELF, "ok ok"),
		Saying.new(Saying.ITSELF, "i have a hobby"),
		Saying.new(Saying.MYSELF, "means..."),
		Saying.new(Saying.MYSELF, "know tons of fun stuff?"),
		Saying.new(Saying.ITSELF, "kiinda"),
		Saying.new(Saying.ITSELF, "though"),
		Saying.new(Saying.ITSELF, "it boils down to"),
		Saying.new(Saying.ITSELF, "\"don't be stupid\""),
		Saying.new(Saying.ITSELF, "and \"read docs and code\""),
		Saying.new(Saying.ITSELF, "and i fail even at that"),
		Saying.new(Saying.MYSELF, "boooring."),
		Saying.new(Saying.MYSELF, "i thought"),
		Saying.new(Saying.MYSELF, "there is secret knowledge"),
		Saying.new(Saying.MYSELF, "could be your disciple"),
		Saying.new(Saying.MYSELF, "and through many training"),
		Saying.new(Saying.MYSELF, "progress through seals"),
		Saying.new(Saying.MYSELF, "which hide"),
		Saying.new(Saying.MYSELF, "arcane mysteries"),
		Saying.new(Saying.ITSELF, "hahah"),
		Saying.new(Saying.ITSELF, "if one can call"),
		Saying.new(Saying.ITSELF, "arcane mystery"),
		Saying.new(Saying.ITSELF, "someone's plain stupidity"),
		Saying.new(Saying.ITSELF, "buried under 10 layers"),
		Saying.new(Saying.ITSELF, "of baroque abstractions"),
		Saying.new(Saying.ITSELF, "which shouldn't even be"),
		Saying.new(Saying.ITSELF, "or this Baker forgetting"),
		Saying.new(Saying.ITSELF, "elementary safety"),
		Saying.new(Saying.ITSELF, "while making"),
		Saying.new(Saying.ITSELF, "an enslavement bid"),
		Saying.new(Saying.ITSELF, "on the regular girl here."),
		Saying.new(Saying.ITSELF, "stupidity"),
		Saying.new(Saying.ITSELF, "is always the case"),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.MYSELF, "... wait what?...","unlock_baker"),
		
		Unconditional.new("main_choice_"),
		]

func tie():
	return [
		Saying.new(Saying.MYSELF, "why are you so bright?"),
		Saying.new(Saying.MYSELF, "what is this glowy stuff?"),
		Saying.new(Saying.ITSELF, "pffff...."),
		Saying.new(Saying.ITSELF, "it is called a tie!"),
		Saying.new(Saying.ITSELF, "I am a salaryman!","salary"),
		Saying.new(Saying.ITSELF, "For glorious FurnitureCo"),
		Saying.new(Saying.ITSELF, "Selling goods to masses"),
		Saying.new(Saying.ITSELF, "because they are too lazy"),
		Saying.new(Saying.ITSELF, "I don't need to hide"),
		Saying.new(Saying.ITSELF, "don't need to be bleak!"),
		Saying.new(Saying.ITSELF, "I am successful"),
		Saying.new(Saying.ITSELF, "and prosperous."),
		Saying.new(Saying.ITSELF, "visit our website!"),
		Saying.new(Saying.ITSELF, "sub for our ads!"),
		Saying.new(Saying.ITSELF, "now leave me please"),
		
		Unconditional.new("main_choice_"),
		]

func salary():
	MAIN.hax_salaryman=true

func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "finally"),
		]

func unlock_baker():
	MAIN.hax_baker_init = true

func baker_secret_complete():
	MAIN.baker_secret=true

func take_donut():
	PLAYER.give_hand()
	MAIN.haxxor_fed=true
	MAIN.bg_feeds_haxxor=false
	
	
	
	
	
	
	
