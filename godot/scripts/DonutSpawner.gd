extends Spatial

var donutScene = load("res://FlyingDonut.tscn")
var period = 0.2

var r_max = 10
var r_min = 5

func _ready():
	
	randomize()
	
	var timer = Timer.new()
	timer.wait_time = period
	timer.connect("timeout",self,"_on_timeout")
	add_child(timer)
	timer.start()
	
func _on_timeout():
	
	var phi = rand_range(0,2*PI)
	var r = rand_range(r_min,r_max)
	
	spawn_donut(Vector3(r*cos(phi),0,r*sin(phi)))

func spawn_donut(vec):
	var donut = donutScene.instance()
	donut.translation = vec
	add_child(donut)
