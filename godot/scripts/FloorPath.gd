tool

extends Path

class_name FloorPath

var ignore = false

func _ready():
	connect("curve_changed",self,"rectify")

func rectify():
	if ignore:
		ignore=false
		return
	print("snapping stuff to the floor")
	for p in range(curve.get_point_count()):
		var pos = curve.get_point_position(p)
		pos = Vector3(pos.x,0,pos.z)
		ignore=true
		curve.set_point_position(p,pos)