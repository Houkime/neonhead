extends Spatial

var lab 
var tim
var size = 0.019
export var col = Color8(255,255,255)

func _ready():
	scale=Vector3(1,1,1)*size

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if is_instance_valid(lab) and "quit" in lab.text: #uuuugly
			get_tree().quit()

func ask_exit():
	nag ("repeat to quit")

func nag(text = "transport error"):
	lab = Label3D.new()
	lab.text = text
	lab.color = col
	lab.power = 3
	add_child(lab)
	tim = Timer.new()
	tim.wait_time = 2
	add_child(tim)
	tim.start()
	tim.one_shot=true
	tim.connect("timeout",self,"destruct")

func destruct():
	for n in [lab,tim]:
		n.queue_free()
