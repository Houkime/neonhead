extends Human
class_name Priest


func _init(): # we can tilt the chatplane a bit?
	#rmargin=-4
	footprint_size = 2.0
	vmargin = -0.5	
	workplace = "PriestPoint"
	transit_point = "TRANSIT2"

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hello"),
		Saying.new(Saying.ITSELF, "hello little man"),
		Saying.new(Saying.ITSELF, "need something?"),
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("lasagna?","lasagna"),
			Choice.new("fun inside lasagna?","lasagna_fun") if MAIN.knows_lasagna_fun else NULL_CHOICE,
			Choice.new("parents?","parents") if MAIN.parenting_explained else NULL_CHOICE,
			Choice.new("powers?","tech_stack") if MAIN.lasagna_side else NULL_CHOICE,
			Choice.new("agitator","agitator") if MAIN.agitator_side else NULL_CHOICE,
			Choice.new("teach me","teach") if MAIN.teacher_needed else NULL_CHOICE,
			Choice.new("adopt me?","adoption") if MAIN.parenting_explained else NULL_CHOICE,
			Choice.new("your distro?","distro") if MAIN.distrowatch else NULL_CHOICE,
			Choice.new("printed is bad?","printed") if MAIN.knows_printed else NULL_CHOICE,
			Choice.new("who are you?","who_are_you"),
			Choice.new("bye...", "bye_"),
		],	
	]

func lasagna_fun():
	return [
		Saying.new(Saying.MYSELF, "what is inside lasagna?"),
		Saying.new(Saying.MYSELF, "exactly?"),
		Saying.new(Saying.ITSELF, "yummey stuff"),
		Saying.new(Saying.ITSELF, "tomato sauce"),
		Saying.new(Saying.ITSELF, "pasta layers"),
		Saying.new(Saying.ITSELF, "cheese"),
		Saying.new(Saying.MYSELF, "aaaaand?..."),
		Saying.new(Saying.ITSELF, "ah, you're about tech?"),
		Saying.new(Saying.ITSELF, "that's a secret"),
		Saying.new(Saying.ITSELF, "won't tell you"),
		Saying.new(Saying.ITSELF, "my exact setup"),
		Saying.new(Saying.ITSELF, "but basically"),
		Saying.new(Saying.ITSELF, "it is self contained"),
		Saying.new(Saying.ITSELF, "lots of synths"),
		Saying.new(Saying.ITSELF, "weapons and defs"),
		Saying.new(Saying.ITSELF, "very hard to die"),
		Saying.new(Saying.ITSELF, "while you're on it"),
		Saying.new(Saying.ITSELF, "unless from obesity"),
		Saying.new(Saying.ITSELF, "but not a panacea"),
		Saying.new(Saying.ITSELF, "you can have one too"),
		Saying.new(Saying.ITSELF, "all components"),
		Saying.new(Saying.ITSELF, "are foss and oshw"),
		
		Unconditional.new("main_choice_"),
		]

func lasagna():
	return [
		Saying.new(Saying.MYSELF, "why lasagna?"),
		Saying.new(Saying.ITSELF, "just having fun"),
		Saying.new(Saying.ITSELF, "a meme tradition"),
		Saying.new(Saying.MYSELF, "does it rot?"),
		Saying.new(Saying.ITSELF, "yep"),
		Saying.new(Saying.ITSELF, "eat it every now and then"),
		Saying.new(Saying.ITSELF, "then resynthesise"),
		Saying.new(Saying.ITSELF, "though fills me too fast"),
		Saying.new(Saying.ITSELF, "neccessitates friends"),
		Saying.new(Saying.ITSELF, "which is a good thing"),
		Saying.new(Saying.ITSELF, "and it is yummey too!"),
		Unconditional.new("main_choice_"),
		]

func teach():
	return [
		Saying.new(Saying.MYSELF, "can you teach me?"),
		Saying.new(Saying.ITSELF, "about tech stack?"),
		Saying.new(Saying.ITSELF, "...man, it's loooong..."),
		Saying.new(Saying.MYSELF, "please?"),
		Saying.new(Saying.ITSELF, "... but"),
		Saying.new(Saying.ITSELF, "there is a church"),
		Saying.new(Saying.ITSELF, "on the Free Node"),
		Saying.new(Saying.ITSELF, "near Church of Emacs"),
		Saying.new(Saying.ITSELF, "you'll never miss it"),
		Saying.new(Saying.ITSELF, "smells majestic"),
		Saying.new(Saying.ITSELF, "you can practice there"),
		Saying.new(Saying.ITSELF, "always in need of hands"),
		Saying.new(Saying.ITSELF, "lasagna free"),
		Saying.new(Saying.ITSELF, "also spaghetti"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.MYSELF, "how do i go there?"),
		Saying.new(Saying.ITSELF, "oh... right..."),
		Saying.new(Saying.ITSELF, "here is the map"),
		Saying.new(Saying.ITSELF, "it is 238 lightyears away"),
		Saying.new(Saying.ITSELF, "no diy tunnels"),
		Saying.new(Saying.ITSELF, "for you yet"),
		Saying.new(Saying.ITSELF, "but public links are ok"),
		Saying.new(Saying.ITSELF, "only 3 jumps"),
		Saying.new(Saying.MYSELF, "i mean, i can't go"),
		Saying.new(Saying.MYSELF, "prints an error"),
		Saying.new(Saying.ITSELF, "what error?"),
		Saying.new(Saying.MYSELF, "transport sth..."),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.ITSELF, "oh man"),
		Saying.new(Saying.ITSELF, "show me your settings"),
		Saying.new(Saying.ITSELF, "here..."),
		Saying.new(Saying.ITSELF, "change NNS from this crap"),
		Saying.new(Saying.ITSELF, "they are poorly federated"),
		Saying.new(Saying.ITSELF, "ok, go","enable_warp"),
		Unconditional.new("main_choice_"),
		]

func enable_warp():
	
	MAIN.warp_enabled = true

func childhood_hook():
	
	MAIN.priest_childhood_hook = true

func agitator():
	return [
		Saying.new(Saying.MYSELF, "know agitator?"), #TODO: actually, agitator can ask to help him
		Saying.new(Saying.ITSELF, "oh this guy"), #TODO: and THEN a kid runs into a priest. a bit more logical and assumes less inquisitiveness of a kid.
		#TODO: also, inquisitiveness might be a choice. e.g. a kid may choose to help agi or be more critical towards him.
		Saying.new(Saying.ITSELF, "yea i saw him"),
		Saying.new(Saying.MYSELF, "is it true what he says?"),
		Saying.new(Saying.ITSELF, "kinda"),
		Saying.new(Saying.ITSELF, "people really go missing"),
		Saying.new(Saying.MYSELF, "so... what lasagna says?"),
		Saying.new(Saying.ITSELF, "lasagna says chillax"),
		Saying.new(Saying.ITSELF, "i say the problem is"),
		Saying.new(Saying.ITSELF, "he is not going anywhere"),
		Saying.new(Saying.ITSELF, "also"),
		Saying.new(Saying.ITSELF, "not telling everything"),
		Saying.new(Saying.ITSELF, "you see"),
		Saying.new(Saying.ITSELF, "govs do exist on the net"),
		Saying.new(Saying.ITSELF, "actually"),
		Saying.new(Saying.ITSELF, "quite many of them"),
		Saying.new(Saying.ITSELF, "and"),
		Saying.new(Saying.ITSELF, "they welcome new citizens"),
		Saying.new(Saying.ITSELF, "what he wants"),
		Saying.new(Saying.ITSELF, "is not just a gov"),
		Saying.new(Saying.ITSELF, "rather a global justice"),
		Saying.new(Saying.ITSELF, "an inescapable"),
		Saying.new(Saying.ITSELF, "punishing force"),
		Saying.new(Saying.ITSELF, "which will vanquish evil"),
		#Saying.new(Saying.ITSELF, "keyword \"inescapable\""),
		Saying.new(Saying.MYSELF, "is it possible?"),
		Saying.new(Saying.ITSELF, "not really"),
		Saying.new(Saying.ITSELF, "but even if it was"),
		Saying.new(Saying.ITSELF, "it is a creepy thing"),
		Saying.new(Saying.MYSELF, "why? if everyone agrees..."),
		Saying.new(Saying.ITSELF, "nah, they wont"),
		Saying.new(Saying.ITSELF, "but"),
		Saying.new(Saying.ITSELF, "even if consensus happens"),
		Saying.new(Saying.ITSELF, "this will mean"),
		Saying.new(Saying.ITSELF, "that freedom is no more"),
		Saying.new(Saying.ITSELF, "everything"),
		Saying.new(Saying.ITSELF, "will need to be tracked"),
		Saying.new(Saying.ITSELF, "and controlled"),
		Saying.new(Saying.ITSELF, "for eradication of \"evil\""),
		Saying.new(Saying.ITSELF, "you need to live in hell"),
		Saying.new(Saying.ITSELF, "you will be stripped"),
		Saying.new(Saying.ITSELF, "of your powers"),
		Saying.new(Saying.ITSELF, "crippled"),
		Saying.new(Saying.ITSELF, "on what you can do"),
		Saying.new(Saying.ITSELF, "does one hate \"evil\""),
		Saying.new(Saying.ITSELF, "so much?"),
		Saying.new(Saying.MYSELF, "...powers?...","lasagnaside"),
		
		Unconditional.new("main_choice_"),
		]

func know_printed():
	MAIN.knows_printed = true

func lasagnaside():
	MAIN.lasagna_side = true
	
func printed():
		return[
		
		Saying.new(Saying.MYSELF, "is being printed bad?"),
		Saying.new(Saying.ITSELF, "a bit"),
		Saying.new(Saying.ITSELF, "when people print others"),
		Saying.new(Saying.ITSELF, "for amusement"),
		Saying.new(Saying.ITSELF, "they often leave trackers"),
		Saying.new(Saying.ITSELF, "and have fun watching"),
		Saying.new(Saying.ITSELF, "someone else's life"),
		Saying.new(Saying.MYSELF, "i can do sth with it?"),
		Saying.new(Saying.ITSELF, "yep but can be hard"),
		Saying.new(Saying.ITSELF, "might be hidden deep"),
		Saying.new(Saying.ITSELF, "within body"),
		Saying.new(Saying.ITSELF, "even in a vital organ"),
		Saying.new(Saying.ITSELF, "... if you have one."),
		Saying.new(Saying.ITSELF, "for all i know"),
		Saying.new(Saying.ITSELF, "you might even be a robot"),
		Saying.new(Saying.ITSELF, "...not that it matters"),
		Saying.new(Saying.ITSELF, "and with no parents","know_parents"),
		Saying.new(Saying.ITSELF, "might be sad for smb"),
		Saying.new(Saying.ITSELF, "not for me though"),
		Unconditional.new("main_choice_"),
	
	]
	
	

func distro():
	return[
		Saying.new(Saying.MYSELF, "what disrto"),
		Saying.new(Saying.MYSELF, "do you use?"),
		Saying.new(Saying.ITSELF, "SpatialVortex"),
		Saying.new(Saying.ITSELF, "has no components"),
		Saying.new(Saying.ITSELF, "of proprietary ancestry"),
		Saying.new(Saying.ITSELF, "only pure"),
		Saying.new(Saying.ITSELF, "community projects"),
		Saying.new(Saying.ITSELF, "from start to end"),
		Saying.new(Saying.ITSELF, "though for you"),
		Saying.new(Saying.ITSELF, "it's unlikely important."),
		Saying.new(Saying.ITSELF, "find sth that suits."),
		Saying.new(Saying.ITSELF, "real bad stuff"),
		Saying.new(Saying.ITSELF, "died long ago"),
		
		Unconditional.new("main_choice_"),
	]

func adoption():
	return[
		Saying.new(Saying.MYSELF, "would you adopt a child?"),
		Saying.new(Saying.MYSELF, "like myself?"),
		Saying.new(Saying.ITSELF, "i'm not a parent"),
		Saying.new(Saying.ITSELF, "and never will be"),
		Saying.new(Saying.ITSELF, "children are a burden"),
		Saying.new(Saying.ITSELF, "and i'm a burden to them"),
		Saying.new(Saying.ITSELF, "advice"),
		Saying.new(Saying.ITSELF, "don't look for people"),
		Saying.new(Saying.ITSELF, "people have own lives"),
		Saying.new(Saying.ITSELF, "and agendas"),
		Saying.new(Saying.ITSELF, "for you to be free"),
		Saying.new(Saying.ITSELF, "tech stack is enough"),
		Saying.new(Saying.ITSELF, "your stack is only yours"),
		Saying.new(Saying.ITSELF, "it has only you"),
		
		Unconditional.new("main_choice_"),
	
	]

func parents():
	return[
		
		Saying.new(Saying.MYSELF, "do you have parents?"),
		Saying.new(Saying.ITSELF, "i do"),
		Saying.new(Saying.ITSELF, "but i wish i didn't"),
		Saying.new(Saying.ITSELF, "they were bad people"),
		Saying.new(Saying.ITSELF, "tyrants"),
		Saying.new(Saying.ITSELF, "i escaped from them"),
		Saying.new(Saying.ITSELF, "once i learned the stack"),
		Saying.new(Saying.ITSELF, "removed trackers"),
		Saying.new(Saying.ITSELF, "of parental control"),
		Saying.new(Saying.ITSELF, "and could traverse space"),
		Saying.new(Saying.ITSELF, "and live on my own"),
		Saying.new(Saying.ITSELF, "tech liberated me"),
		Saying.new(Saying.MYSELF, "but why did you say"),
		Saying.new(Saying.MYSELF, "that with no parents"),
		Saying.new(Saying.MYSELF, "might be sad?"),
		Saying.new(Saying.ITSELF, "not all parents bad"),
		Saying.new(Saying.ITSELF, "some parents are good"),
		Saying.new(Saying.ITSELF, "make for a pleasant"),
		Saying.new(Saying.ITSELF, "childhood"),
		Saying.new(Saying.ITSELF, "until you realise"),
		Saying.new(Saying.ITSELF, "that they are intruders"),
		Saying.new(Saying.ITSELF, "into your privacy"),
		Saying.new(Saying.ITSELF, "but at this point"),
		Saying.new(Saying.ITSELF, "you're an adult"),
		Saying.new(Saying.ITSELF, "and should go","know_parents"),
		Unconditional.new("main_choice_"),
	
	]



func know_parents():
	MAIN.parenting_explained = true

func tech_stack():
	MAIN.knows_stack = true
	return [
		Saying.new(Saying.MYSELF, "govs will strip my powers?"),
		Saying.new(Saying.MYSELF, "what powers?"),
		Saying.new(Saying.ITSELF, "m? tech stack that is on you"),
		Saying.new(Saying.ITSELF, "know how to use it?"),
		Saying.new(Saying.MYSELF, "not really..."),
		Saying.new(Saying.ITSELF, "strange..."),
		Saying.new(Saying.ITSELF, "Now that i look at you"),
		Saying.new(Saying.ITSELF, "were you printed?"),
		Saying.new(Saying.MYSELF, "i don't really remember"),
		Saying.new(Saying.ITSELF, "i bet you don't"),
		Saying.new(Saying.ITSELF, "earliest memory?"),
		Saying.new(Saying.MYSELF, "some crazy robot guy"),
		Saying.new(Saying.MYSELF, "who lives in a maze"),
		Saying.new(Saying.ITSELF, "then you probably were","know_printed"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.ITSELF, "lost the thought"),
		Saying.new(Saying.ITSELF, "..."),
		Saying.new(Saying.MYSELF, "tech stack and govs?"),
		Saying.new(Saying.ITSELF, "ah, right"),
		Saying.new(Saying.ITSELF, "tech stack is wonderful"),
		Saying.new(Saying.ITSELF, "includes OS,"),
		Saying.new(Saying.ITSELF, "then a physical toolchain"),
		Saying.new(Saying.ITSELF, "to make basic stuff"),
		Saying.new(Saying.ITSELF, "from sources,"),
		Saying.new(Saying.ITSELF, "and protection"),
		Saying.new(Saying.ITSELF, "all free as in freedom"),
		Saying.new(Saying.ITSELF, "and infinitely hackable."),
		Saying.new(Saying.ITSELF, "Because of that"),
		Saying.new(Saying.ITSELF, "govs cripple them"),
		Saying.new(Saying.ITSELF, "otherwise too easy"),
		Saying.new(Saying.ITSELF, "to blow stuff up"),
		Saying.new(Saying.ITSELF, "btw"),
		Saying.new(Saying.ITSELF, "find a normal distro","distro_search"),
		Saying.new(Saying.ITSELF, "Bakano is crap"), #TODO: in search of a distro :))))))
		Unconditional.new("main_choice_"),
		]

func distro_search():
	MAIN.distrowatch = true


func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "good luck man"),
		]

func who_are_you():

	return [
		Saying.new(Saying.MYSELF, "Who are you?"),
		Saying.new(Saying.ITSELF, "I am priest Richard"),
		Saying.new(Saying.ITSELF, "of Laid Back Lasagna"),
		Saying.new(Saying.ITSELF, "... Head is a true wordsmith"),
		Unconditional.new("main_choice_"),
	]

func take_donut():
	PLAYER.give_hand()
	MAIN.girl_fed=true
	MAIN.baker_feeds_girl=false
