extends JumpPad

onready var pair = $"../JumpPadIn"

func escape():
	
	if find_parent("MAIN").dna_scanned:
		get_tree().change_scene("res://END7.tscn")
	else:
		get_tree().change_scene("res://END1.tscn")

func _on_Area_body_entered(body):
	#print(body.name+" entered JUMPZONE")
	if body.name=="Player":
		if !pair.will_do_it:
			take_responsibility()
			body.flycrazy = true
			pl = body
			
			var action = "uturn" if not find_parent("MAIN").warp_enabled else "escape"
			accelerate(pl,action,Vector3(0,0,0), vec*body.maxflyspeed)
	
	if body.name == "human":
		var host = body.get_parent().get_parent()
		if host.has_method("warp"):
			host.warp(vec)

func uturn():
	print("uturning")
	var topair = pair.global_transform.origin - global_transform.origin
	#print("topair",topair)
	var playerpos = pl.global_transform.origin - global_transform.origin
	#print("playerpos",playerpos)
	var playerpos_correction = topair.dot(playerpos)/topair.length_squared() #it is a ratio value
	#print("correction",playerpos_correction)
	var topair2= pair.translation - translation
	pl.translation += topair2.rotated(Vector3(0,1,0),get_parent().rotation.y)*(1-playerpos_correction)
	accelerate(pl, "deccel", pl.flyspeed, -pl.flyspeed)
