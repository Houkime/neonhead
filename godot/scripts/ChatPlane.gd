extends Popper

var chat_distance = 0.8
var author_displacement = 3.0
var max_msg = 3
var max_length = 10
var tw = Tween.new()
var messages = Array()
var sc = 1.0/(max_msg+1)*0.4
var chosen = 0
var cur_choice
var choicebar
var refracter = false

var rmargin = 6.0*sc*10
var lmargin = 6.0*sc*10
var vmargin = .2

var mycolor = Color8(200,100,130)

var cur_dialog = Array()
var cur_position = 0

onready var PLAYER = find_parent("MAIN").find_node("Player")
onready var CHATTABLE = get_parent().get_parent()

class_name ChatPlane

func advance():
	if cur_position>=cur_dialog.size():
		PLAYER.end_chat()
		return
	
	var el = cur_dialog[cur_position]
	
	if el is Saying:
		if el.text!="":
			if el.actor==Saying.MYSELF:
				isay(el.text)
			elif el.actor == Saying.DRUNK:
				drunksay(el.text)
			else:
				itsays(el.text)
		else:
			print("skipping line")
			cur_position+=1
			advance()
			return
				
		if el.action and el.action!="":
			CHATTABLE.call_deferred(el.action)
	
	if el is Array:
		choice(el)
	
	if el is Unconditional:
		start(el.text)
		advance()
		return #preventing side-effects when this function DOES end
	
	cur_position+=1
	

func _process(delta):
	if cur_choice and !refracter:
		if Input.is_action_just_pressed("interact"):
			choose()
			return
		if Input.is_action_just_pressed("ui_right"):
			chosen+=1
			update_choice()
		if Input.is_action_just_pressed("ui_left"):
			chosen-=1
			update_choice()
	refracter=false
	if !cur_choice:
		if Input.is_action_just_pressed("interact"):
			advance()
	
func _ready():
	# "trying to summon label")
	randomize()
	if find_parent("MAIN").find_node("Clock"):#enables going from endscreen also
		find_parent("MAIN").find_node("Clock").stop_time()
	PLAYER.find_node("Spatial").visible = false
	rmargin+=CHATTABLE.rmargin
	rmargin+=CHATTABLE.lmargin
	vmargin+=CHATTABLE.vmargin
	
	#get_world().environment.tonemap_exposure=0.8
	
	translation = Vector3(0, -vmargin, -chat_distance)
	
	scale = Vector3(0.09,0.09,0.09)
	cur_dialog = CHATTABLE.init_() # it is a starting dialog not _init
	advance()

func isay(msg):
	var l = message(msg, mycolor)
	var s = l.text.length()
	var margin = lmargin #from the center
	l.translation += Vector3(-margin+(s*(0.5-0.1))*6.0*sc,0,0)
	
func drunksay(msg):
	
	itsays(msg, Color8(50,200,100))
	
func itsays(msg, col = null):
	var l
	
	if col!=null:
		l = message(msg, col)
	else:
		l = message(msg)
		
	var margin = rmargin #from the center
	var s = l.text.length()
	
	l.translation += Vector3(margin-s*(0.5-0.1)*6.0*sc,0,0)

func message(msg, col = Color8(200,200,110)):
	play_sound()
#	print("message started")
	var label = Label3D.new()
	label.text = msg
	label.color = col
	#label.power = 2.5
	#label.power = 2.4
	label.power = 1.8
	label.scale = Vector3(sc,sc,sc)
#	tw.interpolate_property(label,"translation",Vector3(0,-8,0),Vector3(0,0,0),1.0,Tween.TRANS_CUBIC,Tween.EASE_OUT)
#	tw.start()
	add_2_stack(label)
#	print("message ended")
	return label 

func add_2_stack(label):
	label.translation = Vector3(0,-10.0*sc*(float(max_msg)/2.0),0)
	add_child(label)
	
	for m in messages:
		m.translation+=Vector3(0,10.0*sc,0)	
			
	messages.push_front(label)
	if messages.size()>max_msg:
		messages.pop_back().queue_free()

func unpush_stack():
	var t = messages.pop_front()
	t.queue_free()
	
	for m in messages:
		m.translation+=Vector3(0,-10.0*sc,0)
	

func choice(choices):
	var nonzero_choices = Array()
	for c in choices:
		if c.text != "":
			nonzero_choices.append(c)
	cur_choice = nonzero_choices
	chosen = 0
	update_choice()
	
func update_choice():
	chosen=wrapi(chosen,0,cur_choice.size())
	if choicebar:
		unpush_stack()
	choicebar=message(cur_choice[chosen].text)
	generate_arrows(cur_choice.size()>1)

func start(method):
	cur_dialog=CHATTABLE.call(method)
	cur_position=0
	

func choose():
	unpush_stack()
	var target_method=cur_choice[chosen].scenario
	start(target_method)
	choicebar = null
	cur_choice = null
	advance()

func generate_arrows(active):
#	for c in choicebar.get_children():
#		if c is Arrow:
#			c.queue_free()
	var width = choicebar.text.length()*6
	
	var ar1 = Arrow.new()
	var ar2 = Arrow.new()
	
	ar2.left=false
	
	var offset = Vector3(float(width)/2+5.0,0,0)
	
	ar1.translation = -offset
	ar2.translation = offset
	
	for a in [ar1,ar2]:
		a.active = active
		choicebar.add_child(a)

func close():
	
	if not find_parent("MAIN").find_node("Clock").startlock:
		find_parent("MAIN").find_node("Clock").start_time()
	PLAYER.find_node("Spatial").visible = true 
	#TODO: it might make sense to child player hand to a camera. But it will also often look glitchy
	queue_free()
	
