extends Spatial

var def_rotspeed = -0.5
var rotspeed = def_rotspeed


func _process(delta):
	rotation_degrees+=Vector3(0.0,rotspeed,0.0)

func stop(body):
	print("detecting collision")
	if body.name == "Player":
		print("player collision")
		rotspeed = 0 

func start(body):
	if body.name == "Player":
		rotspeed = def_rotspeed 
