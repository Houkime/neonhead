extends Human
class_name BoozeGirl


func _init():
	
	workplace = "Point1"
	transit_point = "TRANSIT1"
	warphook_in = "companion_cloak_in"
	warphook_out = "companion_cloak_out"
	

func companion_cloak_in():
	$Friend.find_node("human_robes").warp(anitime,true)


func companion_cloak_out():
	$Friend.find_node("human_robes").warp(anitime)

func init_():
	
	return [
		Saying.new(Saying.MYSELF, "hi"),
		Saying.new(Saying.ITSELF, "hi... whatever you are"),
		Saying.new(Saying.ITSELF, "kinda dark here"),
		Saying.new(Saying.ITSELF, "you have some vodka?"),
		Unconditional.new("main_choice_"),
	]

func main_choice_():
	
	return [
		[
			Choice.new("i have a donut","donut") if MAIN.baker_feeds_drunkards else NULL_CHOICE,
			Choice.new("nope","nobooze"),
			Choice.new("i have a question","synths"),
			Choice.new("bye...", "bye_"),
		],	
	]


func synths():
	return [
		Saying.new(Saying.MYSELF, "baker says"),
		Saying.new(Saying.MYSELF, "there are"),
		Saying.new(Saying.MYSELF, "opensource synths"),
		Saying.new(Saying.MYSELF, "for food"),
		Saying.new(Saying.MYSELF, "why can't you"),
		Saying.new(Saying.MYSELF, "make yourself vodka?"),
		Saying.new(Saying.DRUNK, "YOU HAVE VOODKA???"),
		Saying.new(Saying.ITSELF, "i can"),
		Saying.new(Saying.ITSELF, "probably"),
		Saying.new(Saying.ITSELF, "if i sit with it"),
		Saying.new(Saying.DRUNK, "MASTAAR!"),
		Saying.new(Saying.ITSELF, "but truth is"),
		Saying.new(Saying.ITSELF, "it is not about vodka"),
		Saying.new(Saying.DRUNK, "WHAAAAAA?!"),
		Saying.new(Saying.ITSELF, "it is a social stuff"),
		Saying.new(Saying.ITSELF, "we hang out"),
		Saying.new(Saying.DRUNK, "UMMM"),
		Saying.new(Saying.DRUNK, "PEEEEEEEPL"),
		Saying.new(Saying.DRUNK, "COMPLEEEEX"),
		Saying.new(Saying.ITSELF, "i like her"),
		Saying.new(Saying.DRUNK, "FLAAATTTRER"),
		Saying.new(Saying.MYSELF, "ok..."),
		Saying.new(Saying.ITSELF, "you better go"),
		
		]

func nobooze():
	return [
		Saying.new(Saying.MYSELF, "nope"),
		Saying.new(Saying.ITSELF, "shame"),
		Saying.new(Saying.DRUNK, "SHEEEEM!"),
		Saying.new(Saying.DRUNK, "NEED VODKA!"),
		Saying.new(Saying.DRUNK, "NOBODY HAS VODKA!"),
		Saying.new(Saying.DRUNK, "WRONG PEOPLE!"),
		Saying.new(Saying.ITSELF, "you better go"),
		Saying.new(Saying.MYSELF, "ok..."),
		Saying.new(Saying.ITSELF, "... this baker"),
		Saying.new(Saying.ITSELF, "wanna kill us probably"), #baker has red eyes while they are there. probably shakes too
		]
	


func donut():
	return [
		Saying.new(Saying.MYSELF, "baker sent a donut"),
		Saying.new(Saying.ITSELF, "with cyanide?"),
		Saying.new(Saying.MYSELF, "hmmm"),
		Saying.new(Saying.MYSELF, "idk actually"),
		Saying.new(Saying.ITSELF, "seems like radioactive"),
		Saying.new(Saying.MYSELF, "..."),
		Saying.new(Saying.ITSELF, "thx"),
		Saying.new(Saying.DRUNK, "WHO NEEDS DONUTS?!!"),
		Saying.new(Saying.DRUNK, "NEED VODKA!"),
		Saying.new(Saying.ITSELF, "if you don't like it"),
		Saying.new(Saying.ITSELF, "your half goes elswhere"),
		Saying.new(Saying.DRUNK, "WHATEVER!"),
		Saying.new(Saying.ITSELF, "if you don't mind"),
		Saying.new(Saying.ITSELF, "give another half"),
		Saying.new(Saying.ITSELF, "to a guy with a red tie","take_donut"),
		Saying.new(Saying.ITSELF, "when you see him"),
		Saying.new(Saying.ITSELF, "i think he forgets to eat"),
		Saying.new(Saying.MYSELF, "...ok?"),
		]

func bye_():
	return [
		Saying.new(Saying.MYSELF, "need to go"),
		Saying.new(Saying.ITSELF, "godspeed"),
		]

func take_donut():
	PLAYER.give_hand()
	MAIN.drunkards_fed=true
	MAIN.baker_feeds_drunkards=false
	PLAYER.take("HalfDonut")
	MAIN.bg_feeds_haxxor = true
