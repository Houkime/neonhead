extends Spatial

class_name Popper

var message_sound = preload("res://message.ogg")
var prev_sp

func play_sound():
	if is_instance_valid(prev_sp):
		prev_sp.pitch_scale = 0.4 #LIFEHACK!!! No soundlevel rip!!!
	
	var sp =SDStreamPlayer3D.new()
	sp.unit_size = 0.1
	sp.stream = message_sound
	
	add_child(sp)
	sp.play()
	prev_sp = sp
