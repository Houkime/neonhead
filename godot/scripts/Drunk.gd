extends Swayer

class_name Drunk

export var power_freq = 0.3
var power_phase = 0
var max_power

func _ready():
	var eyes = find_node("eyes") as MeshInstance
	eyes.set_surface_material(0,mat)
	max_power = mat.emission_energy
	
func _process(delta):
	var p = abs(sin(power_phase)*max_power)
	power_phase=wrapf(power_phase+delta*6.28*power_freq,0,6.28)
	mat.emission_energy = p
