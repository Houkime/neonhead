extends Label3D

class_name EPlace

var stay = 0
export var size = 0.05
var col = Color8(40,180,200)

func _init():
	text = "e"
	color = col
	power = 3
	rotspeed = 6

func _ready():
	scale=Vector3(1,1,1)*size
	
func _process(delta):
	if visible:
		stay-=1
		if stay<=0 : 
			stay=0
			visible = false
		elif stay>4 : 
			stay=4
		rotation_degrees+=Vector3(0.0,rotspeed,0.0)
