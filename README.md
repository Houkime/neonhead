# NeonHead

An extremely nerdy first-person game about freedoms in a FOSS and OSHW dominated world.  

Itch page: https://houkime.itch.io/neonhead

Thanks to Bojangles from Newgrounds for the main theme.
https://www.newgrounds.com/audio/listen/853949

##### Fair Warning:  

Some people might be offended by contents.   
Although I would say that the plot is very kindhearted (at least at the moment i am writing this).  

### Current state:

Mostly ready. Playable, winnable and losable.  

### Licensing

* "Sad Chill" theme belongs to Bojangles (see README in music) and is used here with his permisson. For commercial use contact him/her.
* Code and other assets are licensed under MIT (see LICENSE) and thus can be used with no real restrictions.

### Building from sources

 * grab the latest Godot engine  
    if this repo is not too old preferably from git. That's how it was made.
 * play it right away
    * to play from from editor `godot godot/godot.project` and push the play button on the top-right.
      * Or there is a fully GUI path where you launch godot and import the project.godot from the clone folder etc. etc.
    * you can also launch the game from CLI but you will still need to open editor briefly to import assets (no CLI option for this yet :(( ).
      ```
      godot godot/godot.project
      *quit editor when it finishes importing. Unfortunately -q flag is useless for waiting till import finishes and exiting*
      godot godot/Main.tscn
      ```
 * alternatively, configure Linux target  
    for stable Godot releases you can use template manager to fetch templates, for git you will need to compile your own templates.
 * export
 
 Web version for now lives on its separate branch  
 In some time I will probably make an automated git build for Arch Linux's AUR.  
 Although it will most probably mean compiling engine as well in a local folder since template management in Godot is wonky.

### Binary releases:

None yet but they will end up here and on Itch, also maybe Lutris and whatever you propose in issues.

### Tools used:

* Godot as an engine
* Klystrack foss synth for some sounds (.kt extension)
* Goxel for voxel stuff (which is literally almost every 3D asset)
* Blender for sky